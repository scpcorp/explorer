Run in development mode
-----------------------

```shell
# Start database
docker-compose -f docker-compose.dev.yml up -d
# Apply the migration
PGPASSWORD=123 psql -h localhost -p 5402 -U testuser -d testdb -a \
  -f migration/0001-init.sql
# Run explorer, consensus and gateway files will be stored in $NODE_DIR
NODE_DIR=.node/ ./run-dev.sh
```
