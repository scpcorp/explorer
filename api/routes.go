package api

import (
	"net/http"

	"github.com/starius/api2"
)

func Routes(s server) []api2.Route {
	t := &api2.JsonTransport{}
	t.Errors = map[string]error{
		"InternalServerError": &InternalServerError{},
	}
	return []api2.Route{
		{
			Method:    http.MethodGet,
			Path:      "/contract-stats",
			Handler:   api2.Method(&s, "ContractStats"),
			Transport: t,
		},
	}
}
