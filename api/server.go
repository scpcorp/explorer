package api

import (
	"context"
	"time"

	"gitlab.com/scpcorp/explorer/domain"
	"gitlab.com/scpcorp/explorer/util"
	"go.uber.org/zap"
)

var timeNow = time.Now

var _ server = (*Server)(nil)

type server interface {
	ContractStats(context.Context, *ContractStatsRequest) (*ContractStatsResponse, error)
}

type Server struct {
	l *zap.Logger
	r repository
}

//go:generate moq -out mock_repository_test.go . repository
type repository interface {
	ContractStats(ctx context.Context, before time.Time, limit int) ([]domain.ContractStat, error)
}

func NewServer(l *zap.Logger, r repository) *Server {
	return &Server{l, r}
}

// API: ContractStats

const (
	contractStatsLimitMax     = 500
	contractStatsLimitDefault = 30
)

type ContractStatsRequest struct {
	Before string `query:"before"`
	Limit  int    `query:"limit"`
}

type ContractStatsResponse struct {
	Stats []ContractStat `json:"stats"`
	Next  string         `json:"next,omitempty"`
}

type ContractStat struct {
	Date               string  `json:"date"`
	Contracts          int     `json:"contracts"`
	RawStorage         int     `json:"raw_storage"`
	HostCollateralScp  float64 `json:"host_collateral_scp"`
	RenterAllowanceScp float64 `json:"renter_allowance_scp"`
	RevenueScp         float64 `json:"revenue_scp"`
	HostCollateralUsd  float64 `json:"host_collateral_usd"`
	RenterAllowanceUsd float64 `json:"renter_allowance_usd"`
	RevenueUsd         float64 `json:"revenue_usd"`
}

func (s *Server) ContractStats(ctx context.Context, req *ContractStatsRequest) (resp *ContractStatsResponse, err error) {
	requestedAt := timeNow()
	l := s.l.Named("ContractStats")
	l.Info("request", zap.Any("req", req))
	defer func() {
		l = l.With(zap.Duration("took", time.Since(requestedAt)))
		if err == nil {
			// full response is too big to log
			l.Info("response",
				zap.Int("stats_len", len(resp.Stats)),
				zap.String("next", resp.Next))
		} else {
			l.Error("response error", zap.Error(err))
		}
	}()

	bocd := util.BeginningOfUtcDay(timeNow())
	before := bocd
	if req.Before != "" {
		var err error
		before, err = time.Parse("2006-01-02", req.Before)
		if err != nil {
			l.Error("cannot parse before param",
				zap.String("value", req.Before),
				zap.Error(err))
			return nil, &InternalServerError{}
		}
		if before.After(bocd) {
			before = bocd
			l.Warn("before cannot be in the future",
				zap.String("given", req.Before),
				zap.Time("will_use", before))
		}
	}

	limit := contractStatsLimitDefault
	if req.Limit != 0 {
		if req.Limit < 1 {
			limit = 1
			l.Warn("too small limit",
				zap.Int("given", req.Limit),
				zap.Int("will_use", limit))
		} else if req.Limit > contractStatsLimitMax {
			limit = contractStatsLimitMax
			l.Warn("too big limit",
				zap.Int("given", req.Limit),
				zap.Int("will_use", limit))
		} else {
			limit = req.Limit
		}
	}

	queryLimit := limit + 1
	stats, err := s.r.ContractStats(ctx, before, queryLimit)
	if err != nil {
		l.Error("cannot get contract stats",
			zap.Time("before", before),
			zap.Int("queryLimit", queryLimit),
			zap.Error(err))
		return nil, &InternalServerError{}
	}

	resp = new(ContractStatsResponse)

	if len(stats) == queryLimit {
		stats = stats[0 : len(stats)-1]
		resp.Next = stats[len(stats)-1].Date.In(time.UTC).Format("2006-01-02")
	}

	for _, stat := range stats {
		resp.Stats = append(resp.Stats, ContractStat{
			Date:               stat.Date.In(time.UTC).Format("2006-01-02"),
			Contracts:          stat.Contracts,
			RawStorage:         stat.Bytes,
			HostCollateralScp:  util.CurrencyToScpFloat(stat.HostCollateral),
			RenterAllowanceScp: util.CurrencyToScpFloat(stat.RenterAllowance),
			RevenueScp:         util.CurrencyToScpFloat(stat.Revenue),
			HostCollateralUsd:  stat.HostCollateralUsd,
			RenterAllowanceUsd: stat.RenterAllowanceUsd,
			RevenueUsd:         stat.RevenueUsd,
		})
	}

	return resp, nil
}
