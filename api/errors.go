package api

import "net/http"

type InternalServerError struct{}

func (e *InternalServerError) Error() string {
	return "InternalServerError"
}

func (e *InternalServerError) HttpCode() int {
	return http.StatusInternalServerError
}
