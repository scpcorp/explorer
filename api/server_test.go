package api

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
	"gitlab.com/scpcorp/explorer/logger"
)

func TestServer_ContractStats(t *testing.T) {
	t.Cleanup(func() { timeNow = time.Now })
	s := newServer()
	var ise *InternalServerError
	r_ := s.r.(*repositoryMock)

	timeNow = func() time.Time {
		return time.Date(2024, time.October, 1, 12, 0, 0, 0, time.UTC)
	}

	// Test: repository returns error, default request parameters
	r_.ContractStatsFunc = func(ctx context.Context, before time.Time, limit int) ([]domain.ContractStat, error) {
		require.Equal(t, time.Date(2024, time.October, 1, 0, 0, 0, 0, time.UTC), before)
		require.Equal(t, 31, limit)
		return nil, errors.New("hello")
	}
	_, err := s.ContractStats(context.Background(), &ContractStatsRequest{})
	require.ErrorAs(t, err, &ise)
	require.Len(t, r_.ContractStatsCalls(), 1)

	// Test: ok
	r_.ContractStatsFunc = func(ctx context.Context, before time.Time, limit int) ([]domain.ContractStat, error) {
		require.Equal(t, time.Date(2024, time.October, 1, 0, 0, 0, 0, time.UTC), before)
		require.Equal(t, 31, limit)
		return []domain.ContractStat{
			{
				Date:               time.Date(2024, time.September, 30, 0, 0, 0, 0, time.UTC),
				Contracts:          100,
				Bytes:              100e6,
				HostCollateral:     types.NewCurrency64(100),
				RenterAllowance:    types.NewCurrency64(101),
				Revenue:            types.NewCurrency64(102),
				HostCollateralUsd:  10,
				RenterAllowanceUsd: 11,
				RevenueUsd:         12,
			},
			{
				Date:               time.Date(2024, time.September, 29, 0, 0, 0, 0, time.UTC),
				Contracts:          200,
				Bytes:              200e6,
				HostCollateral:     types.NewCurrency64(200),
				RenterAllowance:    types.NewCurrency64(201),
				Revenue:            types.NewCurrency64(202),
				HostCollateralUsd:  20,
				RenterAllowanceUsd: 21,
				RevenueUsd:         22,
			},
		}, nil
	}
	resp, err := s.ContractStats(context.Background(), &ContractStatsRequest{})
	require.NoError(t, err)
	require.Equal(t, []ContractStat{
		{
			Date:               "2024-09-30",
			Contracts:          100,
			RawStorage:         100e6,
			HostCollateralScp:  100e-27,
			RenterAllowanceScp: 101e-27,
			RevenueScp:         102e-27,
			HostCollateralUsd:  10,
			RenterAllowanceUsd: 11,
			RevenueUsd:         12,
		},
		{
			Date:               "2024-09-29",
			Contracts:          200,
			RawStorage:         200e6,
			HostCollateralScp:  200e-27,
			RenterAllowanceScp: 201e-27,
			RevenueScp:         202e-27,
			HostCollateralUsd:  20,
			RenterAllowanceUsd: 21,
			RevenueUsd:         22,
		},
	}, resp.Stats)
	require.Equal(t, "", resp.Next)
	require.Len(t, r_.ContractStatsCalls(), 2)

	// Test: `before` is set to beginning of current day, if it is in the future
	r_.ContractStatsFunc = func(ctx context.Context, before time.Time, limit int) ([]domain.ContractStat, error) {
		require.Equal(t, time.Date(2024, time.October, 1, 0, 0, 0, 0, time.UTC), before)
		return []domain.ContractStat{}, nil
	}
	_, err = s.ContractStats(context.Background(), &ContractStatsRequest{
		Before: "2024-10-03",
	})
	require.NoError(t, err)
	require.Len(t, r_.ContractStatsCalls(), 3)

	// Test: `before` parsing error
	_, err = s.ContractStats(context.Background(), &ContractStatsRequest{
		Before: "2024-10-33",
	})
	require.ErrorAs(t, err, &ise)
	require.Len(t, r_.ContractStatsCalls(), 3) // not called

	// Test: negative `limit`
	r_.ContractStatsFunc = func(ctx context.Context, before time.Time, limit int) ([]domain.ContractStat, error) {
		require.Equal(t, 2, limit)
		return []domain.ContractStat{}, nil
	}
	_, err = s.ContractStats(context.Background(), &ContractStatsRequest{
		Limit: -1,
	})
	require.NoError(t, err)
	require.Len(t, r_.ContractStatsCalls(), 4)

	// Test: too big `limit`
	r_.ContractStatsFunc = func(ctx context.Context, before time.Time, limit int) ([]domain.ContractStat, error) {
		require.Equal(t, 501, limit)
		return []domain.ContractStat{}, nil
	}
	_, err = s.ContractStats(context.Background(), &ContractStatsRequest{
		Limit: 1000,
	})
	require.NoError(t, err)
	require.Len(t, r_.ContractStatsCalls(), 5)

	// Test: `next` is set
	r_.ContractStatsFunc = func(ctx context.Context, before time.Time, limit int) ([]domain.ContractStat, error) {
		require.Equal(t, 4, limit)
		return []domain.ContractStat{
			{Date: time.Date(2024, time.October, 1, 0, 0, 0, 0, time.UTC)},
			{Date: time.Date(2024, time.September, 30, 0, 0, 0, 0, time.UTC)},
			{Date: time.Date(2024, time.September, 29, 0, 0, 0, 0, time.UTC)},
			{Date: time.Date(2024, time.September, 28, 0, 0, 0, 0, time.UTC)},
		}, nil
	}
	resp, err = s.ContractStats(context.Background(), &ContractStatsRequest{
		Limit: 3,
	})
	require.NoError(t, err)
	require.Len(t, resp.Stats, 3)
	require.Equal(t, "2024-10-01", resp.Stats[0].Date)
	require.Equal(t, "2024-09-30", resp.Stats[1].Date)
	require.Equal(t, "2024-09-29", resp.Stats[2].Date)
	require.Equal(t, "2024-09-29", resp.Next)
	require.Len(t, r_.ContractStatsCalls(), 6)
}

func newServer() *Server {
	l := logger.NewDebug()
	r := new(repositoryMock)
	return NewServer(l, r)
}
