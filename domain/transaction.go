package domain

import (
	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
)

const (
	ThingCoinInput        = "CI"
	ThingCoinOutput       = "CO"
	ThingContract         = "C"
	ThingContractRevision = "CR"
	ThingFundInput        = "FI"
	ThingFundOutput       = "FO"
)

type Transaction struct {
	ID      types.TransactionID
	BlockID types.BlockID
	TxType  modules.TXType
}

// TransactionThing represents indexes of various things of a transaction,
// because indexes of those thing sometimes matter. For example, last input
// of a transaction that forms a contract is host's collateral.
type TransactionThing struct {
	TransactionID types.TransactionID
	Thing         string
	Index         int
	ThingID       crypto.Hash
}
