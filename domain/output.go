package domain

import (
	"fmt"

	"gitlab.com/scpcorp/ScPrime/types"
)

const (
	OutputTypeMinerPayout  = "MNR"
	OutputTypeClaim        = "CLM"
	OutputTypeDevClaim     = "DEV"
	OutputTypeValidRenter  = "VR"
	OutputTypeValidHost    = "VH"
	OutputTypeMissedRenter = "MR"
	OutputTypeMissedHost   = "MH"
	OutputTypeMissedVoid   = "MV"

	ProofIdxValidRenter  = 0
	ProofIdxValidHost    = 1
	ProofIdxMissedRenter = 0
	ProofIdxMissedHost   = 1
	ProofIdxMissedVoid   = 2
)

type Output struct {
	ID             types.OutputID
	Type           string
	UnlockHash     types.UnlockHash
	CoinValue      types.Currency
	FundaValue     types.Currency
	FundbValue     types.Currency
	MaturityHeight types.BlockHeight

	OutputBlockID       types.BlockID
	OutputHeight        types.BlockHeight
	InputBlockID        *types.BlockID
	InputHeight         *types.BlockHeight
	OutputTransactionID *types.TransactionID
	InputTransactionID  *types.TransactionID
	ContractID          *types.FileContractID
	ContractRevision    uint64
}

func ValidProofType(outputIdx int) string {
	switch outputIdx {
	case ProofIdxValidRenter:
		return OutputTypeValidRenter
	case ProofIdxValidHost:
		return OutputTypeValidHost
	default:
		panic(fmt.Sprintf("Unknown valid proof output idx: %d", outputIdx))
	}
}

func MissedProofType(outputIdx int) string {
	switch outputIdx {
	case ProofIdxMissedRenter:
		return OutputTypeMissedRenter
	case ProofIdxMissedHost:
		return OutputTypeMissedHost
	case ProofIdxMissedVoid:
		return OutputTypeMissedVoid
	default:
		panic(fmt.Sprintf("Unknown missed proof output idx: %d", outputIdx))
	}
}
