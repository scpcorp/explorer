package domain

import (
	"gitlab.com/scpcorp/ScPrime/types"
)

const (
	// LinkTypeSetup indicates a link between unlock hashes of inputs and
	// outputs of a setup transaction (modules.TXTypeSetup).
	LinkTypeSetup = "S"

	// LinkTypeAnnouncement indicates a link between unlock hashes of
	// outputs that were used in announcements of a same public key.
	LinkTypeAnnouncement = "A"

	// LinkTypeDefrag indicates a link between inputs and outputs of a
	// defrag transaction.
	LinkTypeDefrag = "D"
)

// Link indicates that both addresses belong to one wallet/host.
type Link struct {
	Address1      types.UnlockHash
	Address2      types.UnlockHash
	Type          string
	TransactionID types.TransactionID
	Height        types.BlockHeight
}

// Root is an address with the earliest height of every graph of addresses,
// that are connected together through Link.
type Root struct {
	Address       types.UnlockHash
	Root          types.UnlockHash
	Type          string
	TransactionID types.TransactionID
	Height        types.BlockHeight
}
