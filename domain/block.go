package domain

import (
	"time"

	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
)

type Block struct {
	ID        types.BlockID
	CCID      modules.ConsensusChangeID
	Height    types.BlockHeight
	ParentID  *types.BlockID
	Timestamp time.Time
}
