package domain

import "gitlab.com/scpcorp/ScPrime/types"

type Balance struct {
	Address      types.UnlockHash
	CoinBalance  types.Currency
	FundaBalance types.Currency
	FundbBalance types.Currency
}
