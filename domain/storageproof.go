package domain

import "gitlab.com/scpcorp/ScPrime/types"

type StorageProof struct {
	ContractID    types.FileContractID
	TransactionID types.TransactionID
	BlockID       types.BlockID
	Height        types.BlockHeight
}
