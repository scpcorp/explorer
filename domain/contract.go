package domain

import "gitlab.com/scpcorp/ScPrime/types"

const RenterNewMuse = "NEW"

type Contract struct {
	ID            types.FileContractID
	Revision      uint64
	Filesize      uint64
	WindowStart   types.BlockHeight
	WindowEnd     types.BlockHeight
	Payout        types.Currency
	UnlockHash    types.UnlockHash
	HostPublicKey *types.SiaPublicKey

	BlockID       types.BlockID
	Height        types.BlockHeight
	TransactionID *types.TransactionID
	RevertBlockID *types.BlockID
	RevertHeight  *types.BlockHeight

	ValidRenterPayout    types.Currency
	ValidRenterAddress   types.UnlockHash
	ValidRenterOutputID  types.SiacoinOutputID
	ValidHostPayout      types.Currency
	ValidHostAddress     types.UnlockHash
	ValidHostOutputID    types.SiacoinOutputID
	MissedRenterPayout   types.Currency
	MissedRenterAddress  types.UnlockHash
	MissedRenterOutputID types.SiacoinOutputID
	MissedHostPayout     types.Currency
	MissedHostAddress    types.UnlockHash
	MissedHostOutputID   types.SiacoinOutputID
	MissedVoidPayout     types.Currency
	MissedVoidAddress    types.UnlockHash
	MissedVoidOutputID   types.SiacoinOutputID

	HostCollateral  types.Currency
	RenterAllowance types.Currency
}
