package domain

import (
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
)

type Announcement struct {
	PublicKey     types.SiaPublicKey
	TransactionID types.TransactionID
	DataIndex     int
	BlockID       types.BlockID
	Height        types.BlockHeight
	Address       modules.NetAddress
}

type ArbitraryData struct {
	BlockID       types.BlockID
	TransactionID types.TransactionID
	Data          [][]byte
}
