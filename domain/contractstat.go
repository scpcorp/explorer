package domain

import (
	"time"

	"gitlab.com/scpcorp/ScPrime/types"
)

type ContractStat struct {
	Date               time.Time
	Contracts          int
	Bytes              int
	HostCollateral     types.Currency
	RenterAllowance    types.Currency
	Revenue            types.Currency
	HostCollateralUsd  float64
	RenterAllowanceUsd float64
	RevenueUsd         float64
}
