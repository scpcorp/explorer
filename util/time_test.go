package util

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestBeginningOfUtcDay(t *testing.T) {
	require.Equal(t,
		time.Date(2024, time.October, 1, 0, 0, 0, 0, time.UTC),
		BeginningOfUtcDay(time.Date(2024, time.October, 1, 15, 0, 0, 0, time.UTC)),
	)

	require.Equal(t,
		time.Date(2024, time.October, 1, 0, 0, 0, 0, time.UTC),
		BeginningOfUtcDay(time.Date(2024, time.October, 1, 0, 0, 0, 0, time.UTC)),
	)

	require.Equal(t,
		time.Date(2024, time.September, 30, 0, 0, 0, 0, time.UTC),
		BeginningOfUtcDay(time.Date(2024, time.September, 30, 23, 59, 59, 0, time.UTC)),
	)

	ny, err := time.LoadLocation("America/New_York")
	require.NoError(t, err)

	require.Equal(t,
		time.Date(2024, time.October, 1, 0, 0, 0, 0, time.UTC),
		BeginningOfUtcDay(time.Date(2024, time.October, 1, 19, 0, 0, 0, ny)),
	)

	require.Equal(t,
		time.Date(2024, time.October, 2, 0, 0, 0, 0, time.UTC),
		BeginningOfUtcDay(time.Date(2024, time.October, 1, 20, 0, 0, 0, ny)),
	)
}
