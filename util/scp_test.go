package util

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/scpcorp/ScPrime/types"
)

func TestCurrencyToScpFloat(t *testing.T) {
	a, err := types.NewCurrencyStr("1 SCP")
	require.NoError(t, err)
	require.Equal(t, float64(1), CurrencyToScpFloat(a))

	a, err = types.NewCurrencyStr("123 SCP")
	require.NoError(t, err)
	require.Equal(t, float64(123), CurrencyToScpFloat(a))

	a, err = types.NewCurrencyStr("33 mS")
	require.NoError(t, err)
	require.Equal(t, 0.033, CurrencyToScpFloat(a))

	a, err = types.NewCurrencyStr("15 H")
	require.NoError(t, err)
	require.Equal(t, 15e-27, CurrencyToScpFloat(a))
}
