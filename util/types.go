package util

import (
	"fmt"
	"strings"

	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/types"
)

const (
	specifierEd25519 = "ed25519:"
	pkLengthBytes    = 32
)

func RemovePkSpecifier(pk string) string {
	return strings.TrimPrefix(pk, specifierEd25519)
}

func ScanPublicKey(s string) (types.SiaPublicKey, error) {
	if len(s) != pkLengthBytes*2 {
		return types.SiaPublicKey{}, fmt.Errorf("bad public key length")
	}

	var pk types.SiaPublicKey
	err := pk.LoadString(specifierEd25519 + s)
	if err != nil {
		return pk, fmt.Errorf("parse error %s: %w", s, err)
	}

	return pk, nil
}

func ScanTransactionID(s string) (types.TransactionID, error) {
	var h crypto.Hash
	err := h.LoadString(s)
	if err != nil {
		return types.TransactionID{}, fmt.Errorf("parse error %s: %w", s, err)
	}
	return types.TransactionID(h), nil
}

func ScanBlockID(s string) (types.BlockID, error) {
	var h crypto.Hash
	err := h.LoadString(s)
	if err != nil {
		return types.BlockID{}, fmt.Errorf("parse error %s: %w", s, err)
	}
	return types.BlockID(h), nil
}

func ScanUnlockHash(s string) (types.UnlockHash, error) {
	var h types.UnlockHash
	err := h.LoadString(s)
	if err != nil {
		return h, fmt.Errorf("parse error %s: %w", s, err)
	}
	return h, nil
}

func ScanHash(s string) (crypto.Hash, error) {
	var h crypto.Hash
	err := h.LoadString(s)
	if err != nil {
		return crypto.Hash{}, fmt.Errorf("parse error %s: %w", s, err)
	}
	return h, nil
}
