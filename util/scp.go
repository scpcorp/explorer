package util

import (
	"math/big"

	"gitlab.com/scpcorp/ScPrime/types"
)

func CurrencyToScpFloat(c types.Currency) float64 {
	// hastings * (1 / 1e27)
	scp, _ := new(big.Rat).Mul(
		new(big.Rat).SetInt(c.Big()),
		new(big.Rat).Inv(
			new(big.Rat).SetInt(types.ScPrimecoinPrecision.Big()),
		),
	).Float64()
	return scp
}
