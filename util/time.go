package util

import "time"

func BeginningOfUtcDay(t time.Time) time.Time {
	year, month, day := t.In(time.UTC).Date()
	return time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
}
