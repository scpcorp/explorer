vendor:
	go mod vendor
	modvendor -copy="**/*.c **/*.h **/*.proto" -v

lint:
	go fmt -mod=vendor ./...
	go vet -mod=vendor ./...
	golangci-lint run --timeout=10m ./...

.PHONY: vendor
