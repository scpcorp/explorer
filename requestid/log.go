package requestid

import (
	"context"

	"go.uber.org/zap"
)

const LoggerKey = "requestid"

func Logger(ctx context.Context, l *zap.Logger) (context.Context, *zap.Logger) {
	ctx, reqID := Context(ctx)
	l = l.With(zap.String(LoggerKey, reqID))
	return ctx, l
}

func LoggerNamed(ctx context.Context, l *zap.Logger, name string) (context.Context, *zap.Logger) {
	return Logger(ctx, l.Named(name))
}
