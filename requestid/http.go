package requestid

import (
	"context"
	"net/http"

	"github.com/google/uuid"
)

const Header = "x-request-id"

type Transport struct {
	http.RoundTripper
}

func (t *Transport) RoundTrip(req *http.Request) (*http.Response, error) {
	// "RoundTrip should not modify the request"
	req2 := req.Clone(req.Context())

	_, reqID := Context(req.Context())
	req2.Header.Set(Header, reqID)

	if t.RoundTripper == nil {
		return http.DefaultTransport.RoundTrip(req2)
	}

	return t.RoundTripper.RoundTrip(req2)
}

// Middleware gets requestID from request header Header or creates new ID, adds
// it to the context under ContextKey and sets response header Header with it.
// If you have several middlewares, this one should be executed first, e.g.:
//
//	h := requestid.Middleware(authMiddleware(lastMiddleware(mux)))
func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reqID := r.Header.Get(Header)
		if reqID == "" {
			reqID = uuid.NewString()
		}

		ctx := context.WithValue(r.Context(), ContextKey, reqID)
		r = r.WithContext(ctx)

		w.Header().Set(Header, reqID)

		h.ServeHTTP(w, r)
	})
}
