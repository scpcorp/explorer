package requestid

import (
	"context"

	"github.com/google/uuid"
)

type contextKeyType int

const ContextKey contextKeyType = 0

// Context returns context.Context with assigned request ID, and the ID itself.
func Context(ctx context.Context) (context.Context, string) {
	if reqID := ctx.Value(ContextKey); reqID != nil {
		return ctx, reqID.(string)
	}

	reqID := uuid.NewString()
	return context.WithValue(ctx, ContextKey, reqID), reqID
}
