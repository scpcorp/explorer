package blockchain

import (
	"context"
	"fmt"
	"path/filepath"
	"time"

	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/modules/consensus"
	"gitlab.com/scpcorp/ScPrime/modules/gateway"
	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/requestid"
	"go.uber.org/zap"
)

type service struct {
	l      *zap.Logger
	dir    string
	cancel chan struct{}
	cerr   chan error

	c modules.ConsensusSet
}

func New(l *zap.Logger, dir string) (*service, chan error) {
	s := &service{
		l:      l,
		dir:    dir,
		cancel: make(chan struct{}),
		cerr:   make(chan error),
	}
	return s, s.cerr
}

func (s *service) Start(ctx context.Context) {
	_, l := requestid.Logger(ctx, s.l.Named("Start"))
	l.Info("Starting...")

	// Gateway.
	t := time.Now()
	rpcAddress := "localhost:0"
	g, err := gateway.NewCustomGateway(rpcAddress, true, filepath.Join(s.dir, modules.GatewayDir), modules.ProdDependencies)
	if err != nil {
		s.cerr <- fmt.Errorf("gateway.NewCustomGateway: %w", err)
		return
	}
	l.Info("Gateway created", zap.Duration("took", time.Since(t)))

	// Consensus
	t = time.Now()
	c, cerr2 := consensus.NewCustomConsensusSet(g, true, filepath.Join(s.dir, modules.ConsensusDir), modules.ProdDependencies)
	if err := modules.PeekErr(cerr2); err != nil {
		s.cerr <- fmt.Errorf("consensus.NewCustomConsensusSet: %w", err)
		return
	}
	l.Info("Consensus created", zap.Duration("took", time.Since(t)))

	s.c = c

	go func() {
		for err := range cerr2 {
			s.cerr <- err
		}
	}()
}

func (s *service) Subscribe(subscriber modules.ConsensusSetSubscriber, start modules.ConsensusChangeID) error {
	err := s.c.ConsensusSetSubscribe(subscriber, start, s.cancel)
	if err != nil {
		return fmt.Errorf("c.ConsensusSetSubscribe: %w", err)
	}
	return nil
}

func (s *service) BlockByID(id types.BlockID) (types.Block, types.BlockHeight, bool) {
	return s.c.BlockByID(id)
}

func (s *service) IsSiafundBOutput(id types.SiafundOutputID) (bool, error) {
	return s.c.IsSiafundBOutput(id)
}

func (s *service) Stop() {
	close(s.cancel)
}
