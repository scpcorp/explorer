CREATE SCHEMA explorer;
SET search_path TO explorer;

-- 46 000 000 total supply, 1e27 number of hastings in one scp
-- 1e40 should be enough
CREATE DOMAIN t_coin AS NUMERIC(40);
-- 200 000 000 total supply of spf-a
-- 200 000 000 total supply of spf-b
CREATE DOMAIN t_fund AS INT;

CREATE DOMAIN t_consensus_change_id VARCHAR(64);
CREATE DOMAIN t_unlock_hash VARCHAR(76);
CREATE DOMAIN t_transaction_id VARCHAR(64);
CREATE DOMAIN t_block_id VARCHAR(64);
CREATE DOMAIN t_output_id VARCHAR(64);
CREATE DOMAIN t_contract_id VARCHAR(64);
CREATE DOMAIN t_public_key VARCHAR(64);
CREATE DOMAIN t_height BIGINT;

CREATE TABLE consensus_changes (
    id          BIGSERIAL PRIMARY KEY,
    cc_id       t_consensus_change_id UNIQUE
);

CREATE TABLE blocks (
    id          t_block_id NOT NULL PRIMARY KEY,
    cc_id       t_consensus_change_id NOT NULL REFERENCES consensus_changes(cc_id),
    height      t_height NOT NULL,
    parent_id   t_block_id REFERENCES blocks(id),
    timestamp   TIMESTAMP NOT NULL
);

CREATE INDEX blocks_height ON blocks(height);

CREATE TABLE transactions (
    id          t_transaction_id NOT NULL PRIMARY KEY,
    block_id    t_block_id NOT NULL REFERENCES blocks(id),
    tx_type     INT NOT NULL
);

CREATE TABLE transaction_things (
    transaction_id  t_transaction_id NOT NULL REFERENCES transactions(id),
    thing           VARCHAR(2),
    index           INT NOT NULL,
    thing_id        VARCHAR(76),
    PRIMARY KEY (transaction_id, thing, index)
);

CREATE INDEX transactions_block_id ON transactions(block_id);

CREATE TABLE contracts (
    id              t_contract_id NOT NULL,
    revision        NUMERIC(20) NOT NULL,
    filesize        NUMERIC(20) NOT NULL, -- uint64
    window_start    t_height NOT NULL,
    window_end      t_height NOT NULL,
    payout          t_coin NOT NULL,
    unlock_hash     t_unlock_hash NOT NULL,
    host_public_key t_public_key,
    renter          VARCHAR(3),

    block_id        t_block_id NOT NULL REFERENCES blocks(id),
    block_height    t_height NOT NULL,
    transaction_id  t_transaction_id REFERENCES transactions(id),
    revert_block_id t_block_id REFERENCES blocks(id),
    revert_height   t_height,

    -- Note: outputs below do not exist on-chain upon contract creation,
    -- and only some of them exist upon contract resolution
    valid_renter_payout     t_coin NOT NULL,
    valid_renter_address    t_unlock_hash NOT NULL,
    valid_renter_output_id  t_output_id NOT NULL,
    valid_host_payout       t_coin NOT NULL,
    valid_host_address      t_unlock_hash NOT NULL,
    valid_host_output_id    t_output_id NOT NULL,
    missed_renter_payout    t_coin NOT NULL,
    missed_renter_address   t_unlock_hash NOT NULL,
    missed_renter_output_id t_output_id NOT NULL,
    missed_host_payout      t_coin NOT NULL,
    missed_host_address     t_unlock_hash NOT NULL,
    missed_host_output_id   t_output_id NOT NULL,
    missed_void_payout      t_coin NOT NULL,
    missed_void_address     t_unlock_hash NOT NULL,
    missed_void_output_id   t_output_id NOT NULL,

    host_collateral         t_coin,
    renter_allowance        t_coin,

    PRIMARY KEY (id, revision)
);

CREATE INDEX contracts_block_height ON contracts(block_height);

CREATE TABLE outputs (
    id              t_output_id NOT NULL PRIMARY KEY,
    type            VARCHAR(3) NOT NULL,
    unlock_hash     t_unlock_hash NOT NULL,
    coin_value      t_coin NOT NULL,
    funda_value     t_fund NOT NULL,
    fundb_value     t_fund NOT NULL,
    maturity_height t_height NOT NULL,

    -- block_id when this output was created
    -- TODO creation_block_id
    output_block_id         t_block_id NOT NULL REFERENCES blocks(id),
    output_height           t_height NOT NULL,
    -- TODO spending_block_id
    -- block_id when this output was used as an input, i.e. was spent
    input_block_id          t_block_id REFERENCES blocks(id),
    input_height            t_height,

    output_transaction_id   t_transaction_id REFERENCES transactions(id),
    input_transaction_id    t_transaction_id REFERENCES transactions(id),

    contract_id             t_contract_id,
    contract_revision       NUMERIC(20),
    FOREIGN KEY (contract_id, contract_revision) REFERENCES contracts (id, revision)
);

CREATE INDEX outputs_unlock_hash_output_height ON outputs(unlock_hash, output_height);
CREATE INDEX outputs_unlock_hash_input_height ON outputs(unlock_hash, input_height);
CREATE INDEX outputs_contract_id_output_height ON outputs(contract_id, output_height);
CREATE INDEX outputs_input_transaction_id ON outputs(input_transaction_id);
CREATE INDEX outputs_output_transaction_id ON outputs(output_transaction_id);
CREATE INDEX outputs_output_height ON outputs(output_height);
CREATE INDEX outputs_input_height ON outputs(input_height);
CREATE INDEX outputs_type_unlock_hash ON outputs(type, unlock_hash);
CREATE INDEX unspent_outputs ON outputs(output_height) WHERE input_height IS NULL AND unlock_hash != '000000000000000000000000000000000000000000000000000000000000000089eb0d6a8a69';


CREATE TABLE storage_proofs (
    contract_id     t_contract_id NOT NULL,
    transaction_id  t_transaction_id NOT NULL REFERENCES transactions(id),
    block_id        t_block_id NOT NULL REFERENCES blocks(id),
    height          t_height NOT NULL,
    PRIMARY KEY (contract_id, transaction_id)
);

CREATE INDEX storage_proofs_contract_id_height ON storage_proofs(contract_id, height);

CREATE TABLE balances (
    address         t_unlock_hash NOT NULL PRIMARY KEY,
    coin_balance    t_coin NOT NULL,
    funda_balance   t_fund NOT NULL,
    fundb_balance   t_fund NOT NULL
);

CREATE TABLE arbitrary_data (
    block_id        t_block_id NOT NULL REFERENCES blocks(id),
    transaction_id  t_transaction_id NOT NULL,
    data_index      INT NOT NULL,
    data            BYTEA,
    PRIMARY KEY (block_id, transaction_id, data_index)
);

CREATE INDEX arbitrary_data_transaction_id ON arbitrary_data(transaction_id);

CREATE TABLE announcements (
    public_key      t_public_key NOT NULL,
    transaction_id  t_transaction_id NOT NULL,
    data_index      INT NOT NULL,
    block_id        t_block_id NOT NULL REFERENCES blocks(id),
    height          t_height NOT NULL,
    address         TEXT NOT NULL,
    PRIMARY KEY (public_key, transaction_id, data_index)
);

CREATE INDEX announcements_transaction_id ON announcements(transaction_id);
CREATE INDEX announcements_pubkey_height ON announcements(public_key, height);

CREATE TABLE links (
    address1        t_unlock_hash NOT NULL,
    address2        t_unlock_hash NOT NULL,
    type            VARCHAR(3) NOT NULL,
    transaction_id  t_transaction_id NOT NULL REFERENCES transactions(id),
    height          t_height NOT NULL,
    PRIMARY KEY (address1, address2, type, transaction_id)
);

CREATE TABLE roots (
    address         t_unlock_hash NOT NULL PRIMARY KEY,
    root            t_unlock_hash NOT NULL,
    type            VARCHAR(3) NOT NULL,
    transaction_id  t_transaction_id NOT NULL REFERENCES transactions(id),
    height          t_height NOT NULL
);

CREATE INDEX roots_root ON roots(root);

-- Materialized Views below.

-- Balance of each root address.
CREATE MATERIALIZED VIEW root_balances AS
SELECT
    COALESCE(r.root, b.address) address,
    r.root IS NOT NULL AS is_root,
    SUM(b.coin_balance) coin_balance,
    SUM(b.funda_balance) funda_balance,
    SUM(b.fundb_balance) fundb_balance
FROM balances b
LEFT JOIN roots r ON r.address = b.address
GROUP BY 1, 2
ORDER BY 3 DESC
WITH DATA;

CREATE UNIQUE INDEX ON root_balances (address);

-- Number of roots and number of unknown unlock hashes of consumed outputs
-- (used as inputs) daily.
CREATE MATERIALIZED VIEW daily_user_activity AS
SELECT time, roots_num, all_num-roots_num unknown_addresses_num FROM (
    SELECT
        DATE_TRUNC('day', b.timestamp) time,
        COUNT(DISTINCT r.root) roots_num,
        COUNT(DISTINCT COALESCE(r.root, o.unlock_hash)) all_num
    FROM outputs o
    LEFT JOIN blocks b ON b.id = o.input_block_id
    LEFT JOIN roots r ON r.address = o.unlock_hash
    WHERE o.input_block_id IS NOT NULL
    GROUP BY 1
    ORDER BY 1
) t
WITH DATA;

CREATE UNIQUE INDEX ON daily_user_activity ("time");

CREATE MATERIALIZED VIEW daily_unlock_hash_activity AS
SELECT
    DATE_TRUNC('day', b.timestamp) time,
    COUNT(DISTINCT o.unlock_hash) num
FROM outputs o
LEFT JOIN blocks b ON b.id = o.input_block_id
LEFT JOIN roots r ON r.address = o.unlock_hash
WHERE o.input_block_id IS NOT NULL
GROUP BY 1
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON daily_unlock_hash_activity ("time");

-- Number of transactions daily.
CREATE MATERIALIZED VIEW daily_transactions AS
SELECT
    DATE_TRUNC('day', b.timestamp) time,
    t.tx_type,
    COUNT(t.*)
FROM transactions t
LEFT JOIN blocks b ON t.block_id = b.id
GROUP BY 1, 2
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON daily_transactions ("time", tx_type);

-- Consumed outputs daily.
CREATE MATERIALIZED VIEW daily_inputs AS
SELECT
    DATE_TRUNC('day', b.timestamp) AS "time",
    COUNT(o.*) FILTER (WHERE o.coin_value > 0) AS scp_num,
    SUM(o.coin_value) scp,
    COUNT(o.*) FILTER (WHERE o.funda_value > 0 OR o.fundb_value > 0) AS spf_num,
    SUM(o.funda_value+o.fundb_value) spf
FROM outputs o
LEFT JOIN blocks b ON b.id = o.input_block_id
WHERE o.input_block_id IS NOT NULL
GROUP BY 1
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON daily_inputs ("time");

-- Outputs with the OutputTypeMinerPayout type (MNR).
CREATE MATERIALIZED VIEW block_payouts AS
SELECT
    b.height,
    b.timestamp,
    b.id AS block_id,
    o.id AS output_id,
    o.coin_value AS payout,
    o.unlock_hash
FROM outputs o
LEFT JOIN blocks b ON o.output_block_id = b.id
WHERE o.type = 'MNR'
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON block_payouts (output_id);

-- Daily sums of moved SCP that ended up in a different root address.
CREATE MATERIALIZED VIEW daily_moved_coins AS
WITH changed_value AS (
    SELECT
        DATE_TRUNC('day', b.timestamp) time,
        COALESCE(r.root, o.unlock_hash) address,
        SUM((CASE WHEN tt.thing = 'CI' THEN -1 ELSE 1 END)*o.coin_value) diff
    FROM transactions t
    LEFT JOIN blocks b ON b.id = t.block_id
    LEFT JOIN transaction_things tt ON tt.transaction_id = t.id AND tt.thing IN ('CO', 'CI')
    LEFT JOIN outputs o ON o.id = tt.thing_id
    LEFT JOIN roots r ON r.address = o.unlock_hash
    WHERE t.tx_type = 2 -- TXTypeSCPMove
    GROUP BY 1, 2
)
SELECT time, SUM(diff) diff
FROM changed_value
WHERE diff > 0
GROUP BY 1
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON daily_moved_coins ("time");

-- Daily sums of moved SPF that ended up in a different root address.
CREATE MATERIALIZED VIEW daily_moved_funds AS
WITH changed_value AS (
    SELECT
        DATE_TRUNC('day', b.timestamp) time,
        COALESCE(r.root, o.unlock_hash) address,
        SUM((CASE WHEN tt.thing = 'FI' THEN -1 ELSE 1 END)*(o.funda_value+o.fundb_value)) diff
    FROM transactions t
    LEFT JOIN blocks b ON b.id = t.block_id
    LEFT JOIN transaction_things tt ON tt.transaction_id = t.id AND tt.thing IN ('FO', 'FI')
    LEFT JOIN outputs o ON o.id = tt.thing_id
    LEFT JOIN roots r ON r.address = o.unlock_hash
    WHERE t.tx_type = 3 -- TXTypeSPFMove
    GROUP BY 1, 2
)
SELECT time, SUM(diff) diff
FROM changed_value
WHERE diff > 0
GROUP BY 1
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON daily_moved_funds ("time");

CREATE MATERIALIZED VIEW contract_economy AS
WITH last_rev AS (
    SELECT DISTINCT ON (id) id, revision
    FROM contracts
    ORDER BY id, revision DESC
)
SELECT
    c1.id,
    c1.block_height,
    c1.host_collateral,
    c1.renter_allowance,
    contract_block.timestamp AS block_timestamp,
    COALESCE(
        c1.payout - host_payout.coin_value - renter_payout.coin_value - COALESCE(void_payout.coin_value, 0),
        -- Until there is no final revision (c2), there is no payout outputs.
        (c1.payout *
            CASE
                WHEN c1.block_height <= 54550 THEN 39
                WHEN c1.block_height <= 109000 THEN 150
                WHEN c1.block_height <= 222800 THEN 100
                ELSE 150
            END / 1000
        )
    ) AS spf_tax,
    host_payout.id AS host_payout_id,
    host_payout.coin_value AS host_payout,
    renter_payout.id AS renter_payout_id,
    renter_payout.coin_value AS renter_payout,
    void_payout.id AS void_payout_id,
    void_payout.coin_value AS void_payout,
    payout_block.timestamp AS payout_timestamp,
    payout_block.height AS payout_height
FROM contracts c1
LEFT JOIN blocks contract_block ON contract_block.id = c1.block_id
LEFT JOIN last_rev r2 ON r2.id = c1.id
LEFT JOIN contracts c2 ON c2.id = c1.id AND c2.revision = r2.revision
LEFT JOIN outputs host_payout ON host_payout.id IN (c2.valid_host_output_id, c2.missed_host_output_id)
LEFT JOIN blocks payout_block ON payout_block.id = host_payout.output_block_id
LEFT JOIN outputs renter_payout ON renter_payout.id IN (c2.valid_renter_output_id, c2.missed_renter_output_id)
LEFT JOIN outputs void_payout ON void_payout.id = c2.missed_void_output_id
WHERE c1.revision = 0
WITH DATA;

CREATE UNIQUE INDEX ON contract_economy (id);

CREATE MATERIALIZED VIEW contract_host_revenue_stats AS
WITH daily_revenue AS (
    SELECT
        DATE_TRUNC('day', payout_timestamp) AS "time",
        SUM(host_payout-host_collateral) AS revenue
    FROM contract_economy
    WHERE host_payout > host_collateral
    GROUP BY 1
),
date_series AS (
    SELECT GENERATE_SERIES(
        (SELECT MIN(time) FROM daily_revenue),
        (SELECT MAX(time) FROM daily_revenue),
        INTERVAL '1 day'
    ) AS time
)
SELECT
    time,
    revenue,
    SUM(revenue) OVER (ORDER BY time) AS total
FROM (
    SELECT
        ds.time,
        COALESCE(dr.revenue, 0) AS revenue
    FROM date_series ds
    LEFT JOIN daily_revenue dr ON dr.time = ds.time
) t
GROUP BY 1, 2
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON contract_host_revenue_stats (time);

CREATE MATERIALIZED VIEW transaction_fees AS
SELECT
    t.id,
    b.timestamp,
    SUM(o.coin_value) FILTER(WHERE tt.thing = 'CI')-
    COALESCE(SUM(o.coin_value) FILTER(WHERE tt.thing = 'CO'), 0) fees
FROM transactions t
LEFT JOIN blocks b ON b.id = t.block_id
LEFT JOIN transaction_things tt ON tt.transaction_id = t.id AND tt.thing IN ('CI', 'CO')
LEFT JOIN outputs o ON o.id = tt.thing_id
WHERE
    tx_type NOT IN (4, 5, 6, 7) -- Exclude contract transactions.
GROUP BY 1, 2
HAVING SUM(o.coin_value) FILTER(WHERE tt.thing = 'CI')-
    COALESCE(SUM(o.coin_value) FILTER(WHERE tt.thing = 'CO'), 0) > 0
ORDER BY 2
WITH DATA;

CREATE UNIQUE INDEX ON transaction_fees (id);
CREATE INDEX ON transaction_fees (timestamp);

CREATE MATERIALIZED VIEW daily_median_transaction_fees AS
SELECT
    DATE_TRUNC('day', timestamp) AS "time",
    PERCENTILE_DISC(0.50) WITHIN GROUP (ORDER BY fees) AS "fees"
FROM transaction_fees
GROUP BY 1
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON daily_median_transaction_fees (time);

CREATE MATERIALIZED VIEW contract_fees AS
SELECT
    b.timestamp,
    c.id,
    t.id transaction_id,
    COALESCE(SUM(o.coin_value) FILTER(WHERE tt.thing = 'CI'), 0)-
    COALESCE(SUM(o.coin_value) FILTER(WHERE tt.thing = 'CO'), 0)-
    COALESCE(MAX(CASE WHEN tx_type IN (4, 5) THEN c.payout ELSE 0 END), 0) fees
FROM transactions t
LEFT JOIN blocks b ON b.id = t.block_id
LEFT JOIN transaction_things tt ON tt.transaction_id = t.id AND tt.thing IN ('CI', 'CO')
LEFT JOIN outputs o ON o.id = tt.thing_id
LEFT JOIN contracts c ON c.transaction_id = t.id
WHERE tx_type IN (4, 5, 6, 7)
GROUP BY 1, 2, 3
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON contract_fees (id, transaction_id);
CREATE INDEX ON contract_fees (timestamp);

-- Old contracts which heights intersects with every day's [min(height), max(height)].
CREATE MATERIALIZED VIEW daily_contracts1 AS
WITH last_rev AS (
    SELECT DISTINCT ON (id) id, revision
    FROM contracts
    ORDER BY id, revision DESC
),
contract_ranges AS (
    SELECT
        c1.unlock_hash,
        c1.id,
        c1.payout,
        c1.filesize,
        c1.block_height,
        COALESCE(c2.block_height, c1.window_end) AS end_height,
        c1.host_public_key,
        c1.host_collateral,
        c1.renter_allowance
    FROM contracts c1
    LEFT JOIN last_rev ON last_rev.id = c1.id AND last_rev.revision > 0
    LEFT JOIN contracts c2 ON c2.id = c1.id AND c2.revision = last_rev.revision
    WHERE c1.revision = 0
),
block_ranges AS (
    SELECT
        DATE_TRUNC('day', timestamp) AS "time",
        MIN(height) AS min_height,
        MAX(height) AS max_height
    FROM blocks
    WHERE timestamp < '2024-06-01 00:00:00'
    GROUP BY 1
    ORDER BY 1 DESC
),
all_contracts AS (
    SELECT
        br.time,
        cr.unlock_hash,
        cr.id,
        cr.payout,
        cr.filesize,
        cr.block_height,
        cr.end_height,
        cr.host_public_key,
        cr.host_collateral,
        cr.renter_allowance
    FROM block_ranges br
    LEFT JOIN contract_ranges cr ON br.min_height < cr.end_height AND cr.block_height <= br.max_height
    ORDER BY 1, cr.unlock_hash, cr.id
)
SELECT
    *,
    -- If there is more than one active contract per unlock_hash, the one
    -- with greater block_height becomes "latest".
    CASE
        WHEN RANK() OVER (PARTITION BY time, unlock_hash ORDER BY block_height DESC, id) = 1 THEN TRUE
        ELSE FALSE
    END AS "latest"
FROM all_contracts
WITH DATA;

CREATE UNIQUE INDEX ON daily_contracts1 ("time", unlock_hash, id);
CREATE UNIQUE INDEX ON daily_contracts1 ("time", unlock_hash) WHERE latest;

-- Fresh contracts which heights intersects with every day's [min(height), max(height)].
CREATE MATERIALIZED VIEW daily_contracts2 AS
WITH last_rev AS (
    SELECT DISTINCT ON (id) id, revision
    FROM contracts
    ORDER BY id, revision DESC
),
contract_ranges AS (
    SELECT
        c1.unlock_hash,
        c1.id,
        c1.payout,
        c1.filesize,
        c1.block_height,
        COALESCE(c2.block_height, c1.window_end) AS end_height,
        c1.host_public_key,
        c1.host_collateral,
        c1.renter_allowance
    FROM contracts c1
    LEFT JOIN last_rev ON last_rev.id = c1.id AND last_rev.revision > 0
    LEFT JOIN contracts c2 ON c2.id = c1.id AND c2.revision = last_rev.revision
    WHERE c1.revision = 0
),
block_ranges AS (
    SELECT
        DATE_TRUNC('day', timestamp) AS "time",
        MIN(height) AS min_height,
        MAX(height) AS max_height
    FROM blocks
    WHERE timestamp >= '2024-06-01 00:00:00'
    GROUP BY 1
    ORDER BY 1 DESC
),
all_contracts AS (
    SELECT
        br.time,
        cr.unlock_hash,
        cr.id,
        cr.payout,
        cr.filesize,
        cr.block_height,
        cr.end_height,
        cr.host_public_key,
        cr.host_collateral,
        cr.renter_allowance
    FROM block_ranges br
    LEFT JOIN contract_ranges cr ON br.min_height < cr.end_height AND cr.block_height <= br.max_height
    ORDER BY 1, cr.unlock_hash, cr.id
)
SELECT
    *,
    -- If there is more than one active contract per unlock_hash, the one
    -- with greater block_height becomes "latest".
    CASE
        WHEN RANK() OVER (PARTITION BY time, unlock_hash ORDER BY block_height DESC, id) = 1 THEN TRUE
        ELSE FALSE
    END AS "latest"
FROM all_contracts
WITH DATA;

CREATE UNIQUE INDEX ON daily_contracts2 ("time", unlock_hash, id);
CREATE UNIQUE INDEX ON daily_contracts2 ("time", unlock_hash) WHERE latest;

CREATE VIEW daily_contracts AS
SELECT * FROM daily_contracts1
UNION
SELECT * FROM daily_contracts2;

-- SCP created by miners plus genesis airdrop tx (about 10M SCP).
CREATE MATERIALIZED VIEW total_coins AS
SELECT
    height,
    timestamp,
    SUM(coin_value) OVER (ORDER BY height) AS coin_value
FROM (
SELECT
    b.height,
    b.timestamp,
    SUM(o.coin_value) coin_value
FROM blocks b
LEFT JOIN outputs o ON o.output_height = b.height AND (
    -- Miner Payouts
    o.type = 'MNR' OR
    -- Airdrop genesis transaction
    o.output_transaction_id = '36c15a9e02fec1c33882f3ba48337732c8166c412928d7f00aa8490e0fc09fb6'
)
GROUP BY 1, 2
ORDER BY 1
) t
WITH DATA;

CREATE UNIQUE INDEX ON total_coins (height);

-- This includes burned (by whatever reason, whether it is dev fund, or
-- transported), and excludes unclaimed coins from spf tax.
-- Basically, whatever outputs there are, that are not spent.
-- Value can go down over time, because some SCP used in contracting goes into
-- claims pool ("spf tax"), and are not created on chain right away.
CREATE MATERIALIZED VIEW unspent_coins AS
SELECT
    height,
    timestamp,
    SUM(coin_value) OVER (ORDER BY height) AS coin_value
FROM (
SELECT
    b.height,
    b.timestamp,
    SUM((CASE
        WHEN o.output_height = o.input_height THEN 0
        WHEN b.height = o.output_height THEN 1
        ELSE -1
    END) * o.coin_value) coin_value
FROM blocks b
LEFT JOIN outputs o ON o.output_height = b.height OR o.input_height = b.height
GROUP BY 1, 2
ORDER BY 1
) t
WITH DATA;

CREATE UNIQUE INDEX ON unspent_coins (height);

-- Coins sent to or from burn address. In the latter case, coin_value
-- would be negative.
CREATE MATERIALIZED VIEW coin_burns AS
SELECT
    b.height,
    b.timestamp,
    o.id output_id,
    CASE
        WHEN
            (
                ad.data IS NOT NULL AND
                -- See putSolanaAddressWithPrefix()
                SUBSTRING(data FROM 0 FOR 33) = 'NonSia\000\000\000\000\000\000\000\000\000\000SolanaScpAddr\000\000\000'::BYTEA
            ) OR
            -- Manual transport.
            o.output_transaction_id IN (
                '99dd6495ed5f622829942d7ae7efad690129534a6e5f322966f47fe353054816', -- 1K
                '92a30d1d035f87b32e92df731b6afe42bcd8e0f431ca3a0c3833d1d1c9495d7f'  -- 50K
            )
            THEN 'transport'
        WHEN o.type = 'MNR' AND o.output_height < 293210 THEN 'dev-fund'
        WHEN o.type = 'MNR' AND o.output_height >= 293210 THEN 'dev-fund-solana'
        WHEN o.type = 'MV' THEN 'failed-contracts'
        ELSE 'regular'
    END burn_type,
    SUM((CASE
        WHEN o.output_height = o.input_height THEN 0
        WHEN b.height = o.output_height THEN 1
        ELSE -1
    END) * o.coin_value) coin_value
FROM blocks b
LEFT JOIN outputs o ON o.output_height = b.height OR o.input_height = b.height
LEFT JOIN arbitrary_data ad ON ad.transaction_id = o.output_transaction_id AND data_index = 0
WHERE
    o.unlock_hash = '000000000000000000000000000000000000000000000000000000000000000089eb0d6a8a69' AND
    o.coin_value > 0
GROUP BY 1, 2, 3, 4
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON coin_burns (height, output_id);
CREATE INDEX ON coin_burns (timestamp);

CREATE MATERIALIZED VIEW coin_burns_stats AS
WITH daily_burns AS (
    SELECT
        DATE_TRUNC('day', timestamp) AS "time",
        burn_type,
        SUM(coin_value) AS coin_value
    FROM coin_burns
    GROUP BY 1, 2
    ORDER BY 1, 2
),
date_type_series AS (
    -- Select from this so that final view would not have gaps.
    SELECT time, burn_type
    FROM
        GENERATE_SERIES(
            (SELECT MIN(time) FROM daily_burns),
            (SELECT MAX(time) FROM daily_burns),
            INTERVAL '1 day'
        ) AS time,
        (SELECT DISTINCT burn_type FROM daily_burns) tt
)
SELECT
    "time",
    burn_type,
    coin_value,
    SUM(coin_value) OVER (PARTITION BY burn_type ORDER BY time) AS total
FROM (
    SELECT
        dts.time,
        dts.burn_type,
        COALESCE(db.coin_value, 0) coin_value
    FROM date_type_series dts
    LEFT JOIN daily_burns db ON db.time = dts.time AND db.burn_type = dts.burn_type
) t
GROUP BY 1, 2, 3
ORDER BY 1, 2
WITH DATA;

CREATE UNIQUE INDEX ON coin_burns_stats ("time", burn_type);

CREATE MATERIALIZED VIEW unspent_funds AS
SELECT
    height,
    timestamp,
    SUM(fund_value) OVER (ORDER BY height) AS fund_value
FROM (
SELECT
    b.height,
    b.timestamp,
    SUM((CASE
        WHEN o.output_height = o.input_height THEN 0
        WHEN b.height = o.output_height THEN 1
        ELSE -1
    END) * (o.funda_value+o.fundb_value)) fund_value
FROM blocks b
LEFT JOIN outputs o ON o.output_height = b.height OR o.input_height = b.height
GROUP BY 1, 2
ORDER BY 1
) t
WITH DATA;

CREATE UNIQUE INDEX ON unspent_funds (height);

CREATE MATERIALIZED VIEW fund_burns AS
SELECT
    b.height,
    b.timestamp,
    o.id output_id,
    CASE
        WHEN
            (
                ad.data IS NOT NULL AND
                -- See putSolanaAddressWithPrefix()
                SUBSTRING(data FROM 0 FOR 33) = 'NonSia\000\000\000\000\000\000\000\000\000\000SolanaSpfAddr\000\000\000'::BYTEA
            ) OR
            -- Manual transport.
            o.output_transaction_id IN (
                '9a22914ab63bdb1ece5987aeb8e1c7f76895749283e19cbd73ea372e0323e495', -- 1K
                '587ec44be700b1a69b9dee8db0f6fe5dc5be381f75a8667e7cae59fd62e9d61b'  -- 1499K
            )
            THEN 'transport'
        WHEN
            -- 15M burned manually for airdrop.
            o.output_transaction_id IN (
                '064cdfdd36b117e02ba21b2750155e2c1604842465d8d58ba4c4ac30bb41bb44',
                '9200fd803d8651d08a4f395f219222d40b8b746557332c235e636c4206e5308e'
            )
            THEN 'airdrop'
        ELSE 'regular'
    END burn_type,
    SUM((CASE
        WHEN o.output_height = o.input_height THEN 0
        WHEN b.height = o.output_height THEN 1
        ELSE -1
    END) * (o.funda_value+o.fundb_value)) fund_value
FROM blocks b
LEFT JOIN outputs o ON o.output_height = b.height OR o.input_height = b.height
LEFT JOIN arbitrary_data ad ON ad.transaction_id = o.output_transaction_id AND data_index = 0
WHERE o.unlock_hash = '000000000000000000000000000000000000000000000000000000000000000089eb0d6a8a69' AND (o.funda_value > 0 OR o.fundb_value > 0)
GROUP BY 1, 2, 3, 4
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON fund_burns (height, output_id);
CREATE INDEX ON fund_burns (timestamp);

CREATE MATERIALIZED VIEW fund_burns_stats AS
WITH daily_burns AS (
    SELECT
        DATE_TRUNC('day', timestamp) AS "time",
        burn_type,
        SUM(fund_value) AS fund_value
    FROM fund_burns
    GROUP BY 1, 2
    ORDER BY 1, 2
),
date_type_series AS (
    -- Select from this so that final view would not have gaps.
    SELECT time, burn_type
    FROM
        GENERATE_SERIES(
            (SELECT MIN(time) FROM daily_burns),
            (SELECT MAX(time) FROM daily_burns),
            INTERVAL '1 day'
        ) AS time,
        (SELECT DISTINCT burn_type FROM daily_burns) tt
)
SELECT
    "time",
    burn_type,
    fund_value,
    SUM(fund_value) OVER (PARTITION BY burn_type ORDER BY time) AS total
FROM (
    SELECT
        dts.time,
        dts.burn_type,
        COALESCE(db.fund_value, 0) fund_value
    FROM date_type_series dts
    LEFT JOIN daily_burns db ON db.time = dts.time AND db.burn_type = dts.burn_type
) t
GROUP BY 1, 2, 3
ORDER BY 1, 2
WITH DATA;

CREATE UNIQUE INDEX ON fund_burns_stats ("time", burn_type);

CREATE MATERIALIZED VIEW claim_stats AS
WITH daily_claims AS (
    SELECT
        DATE_TRUNC('day', block_timestamp) AS time,
        SUM(spf_tax) AS spf_tax
    FROM contract_economy
    GROUP BY 1
),
daily_outputs AS (
    SELECT
        DATE_TRUNC('day', b.timestamp) AS time,
        o.type AS output_type,
        SUM(o.coin_value) AS claimed_value
    FROM outputs o
    LEFT JOIN blocks b ON b.id = o.output_block_id
    WHERE o.type IN ('CLM', 'DEV')
    GROUP BY 1, 2
),
date_series AS (
    SELECT GENERATE_SERIES(
        LEAST((SELECT MIN(time) FROM daily_claims), (SELECT MIN(time) FROM daily_outputs)),
        GREATEST((SELECT MAX(time) FROM daily_claims), (SELECT MAX(time) FROM daily_outputs)),
        INTERVAL '1 day'
    ) AS time
)
SELECT
    time,
    spf_tax,
    normal_claimed,
    dev_claimed,
    SUM(spf_tax) OVER (ORDER BY time) AS spf_tax_total,
    SUM(normal_claimed) OVER (ORDER BY time) AS normal_claimed_total,
    SUM(dev_claimed) OVER (ORDER BY time) AS dev_claimed_total
FROM (
    SELECT
        ds.time,
        COALESCE(dc.spf_tax, 0) AS spf_tax,
        COALESCE(o1.claimed_value, 0) AS normal_claimed,
        COALESCE(o2.claimed_value, 0) AS dev_claimed
    FROM date_series ds
    LEFT JOIN daily_claims dc ON dc.time = ds.time
    LEFT JOIN daily_outputs o1 ON o1.time = ds.time AND o1.output_type = 'CLM'
    LEFT JOIN daily_outputs o2 ON o2.time = ds.time AND o2.output_type = 'DEV'
) t
GROUP BY 1, 2, 3, 4
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON claim_stats ("time");

CREATE MATERIALIZED VIEW daily_contracted_filesize AS
SELECT
    time,
    COUNT(*) AS count,
    SUM(filesize) AS filesize
FROM daily_contracts
WHERE latest
GROUP BY 1
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON daily_contracted_filesize ("time");

CREATE MATERIALIZED VIEW daily_contract_funding AS
SELECT
    time,
    SUM(payout) AS payout,
    SUM(host_collateral) AS host_collateral,
    SUM(renter_allowance) AS renter_allowance
FROM daily_contracts
GROUP BY 1
ORDER BY 1
WITH DATA;

CREATE UNIQUE INDEX ON daily_contract_funding ("time");

-- Daily average SCP price with gaps filled with non-null price from previous day.
CREATE MATERIALIZED VIEW daily_scp_price AS
WITH daily_price_w_gaps AS (
    SELECT
        DATE_TRUNC('day', TO_TIMESTAMP(last_updated_at)) time,
        AVG(usd) price
    FROM markets.coingecko_simple
    GROUP BY 1
),
date_series AS (
    SELECT GENERATE_SERIES(
        (SELECT MIN(time) FROM daily_price_w_gaps),
        (SELECT DATE_TRUNC('day', NOW())),
        INTERVAL '1 day'
    ) AS time
),
price_helper AS (
    SELECT
        ds.time,
        dp.price,
        SUM(CASE WHEN dp.price IS NOT NULL THEN 1 END) OVER (ORDER BY ds.time) AS rn
    FROM date_series ds
    LEFT JOIN daily_price_w_gaps dp ON dp.time = ds.time
)
SELECT
    time,
    FIRST_VALUE(price) OVER (PARTITION BY rn) price
FROM price_helper
WITH DATA;

CREATE UNIQUE INDEX ON daily_scp_price ("time");
