package syncer

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
	"gitlab.com/scpcorp/explorer/repository"
	"gitlab.com/scpcorp/explorer/requestid"
	"go.uber.org/zap"
)

const (
	// defragBatchSize defines how many outputs are combined during one defrag.
	// See ScPrime/modules/wallet/consts.go
	defragBatchSize = 35
)

type processor struct {
	l   *zap.Logger
	ctx context.Context
	b   blockchainService
	r   repositoryService
	cc  modules.ConsensusChange

	stats processorStats

	tx repository.Repository
}

type processorStats struct {
	RevertedBlocks int

	AddedBlocks          int
	AddedHeightsList     []types.BlockHeight
	AddedTransactions    int
	AddedOutputs         int
	UsedInputs           int
	AddedContracts       int
	RevertedContracts    int
	TransferedCoinValue  types.Currency
	TransferedFundaValue types.Currency
	TransferedFundbValue types.Currency
	AddedLinks           int

	Took time.Duration
}

func newProcessor(l *zap.Logger, ctx context.Context, b blockchainService, r repositoryService, cc modules.ConsensusChange) *processor {
	return &processor{
		l:   l,
		ctx: ctx,
		b:   b,
		r:   r,
		cc:  cc,
	}
}

var (
	emptyBlockID = types.BlockID{}
)

func (p *processor) Process() error { //nolint:gocyclo
	started := time.Now()

	p.ctx, p.l = requestid.Logger(
		p.ctx,
		p.l.Named("Processor"),
	)
	p.l.Debug("processing...",
		zap.String("CCID", p.cc.ID.String()))

	err := p.r.Transaction(p.ctx, func(r repository.Repository) error {
		p.tx = r

		err := p.processRevertedBlocks()
		if err != nil {
			return fmt.Errorf("processRevertedBlocks: %w", err)
		}
		p.stats.RevertedBlocks = len(p.cc.RevertedBlocks)

		err = p.tx.AddConsensusChange(p.ctx, p.cc.ID)
		if err != nil {
			return fmt.Errorf("r.AddConsensusChange ccID=%s: %w", p.cc.ID, err)
		}

		for i, appliedBlock := range p.cc.AppliedBlocks {
			appliedDiffs := p.cc.AppliedDiffs[i]

			// TODO calculate height without calling BlockByID on every block.
			// see spd's modules/host
			_, height, ok := p.b.BlockByID(appliedBlock.ID())
			if !ok {
				return fmt.Errorf("b.BlockByID blockID=%s not found", appliedBlock.ID())
			}

			p.stats.AddedHeightsList = append(p.stats.AddedHeightsList, height)
			p.l.Info("Height", zap.Uint64("height", uint64(height)))

			// Add block.
			block := domain.Block{
				ID:     appliedBlock.ID(),
				CCID:   p.cc.ID,
				Height: height,
				//ParentID: TBD,
				Timestamp: time.Unix(int64(appliedBlock.Timestamp), 0).UTC(),
			}
			if appliedBlock.ParentID != emptyBlockID {
				block.ParentID = &appliedBlock.ParentID
			}
			err := p.tx.AddBlock(p.ctx, block)
			if err != nil {
				return fmt.Errorf("r.AddBlock blockID=%s: %w", block.ID, err)
			}

			for _, transaction := range appliedBlock.Transactions {
				if len(transaction.SiacoinInputs) == 0 &&
					len(transaction.SiacoinOutputs) == 0 &&
					len(transaction.SiafundInputs) == 0 &&
					len(transaction.SiafundOutputs) == 0 &&
					len(transaction.MinerFees) == 0 &&
					len(transaction.FileContracts) == 0 &&
					len(transaction.FileContractRevisions) == 0 &&
					len(transaction.StorageProofs) == 0 &&
					len(transaction.ArbitraryData) > 0 {
					// Mining pool create empty transactions with arbitrary
					// data. This will be saved in processArbitrary.
					continue
				}

				transaction := transaction
				tx := domain.Transaction{
					ID:      transaction.ID(),
					BlockID: block.ID,
					TxType:  modules.TransactionType(&transaction),
				}
				err := p.tx.AddTransaction(p.ctx, tx)
				if err != nil {
					return fmt.Errorf("tx.AddTransaction blockID=%s transactionID=%s: %w", block.ID, tx.ID, err)
				}
				p.stats.AddedTransactions++

				for _, fcr := range transaction.FileContractRevisions {
					contractID := fcr.ParentID
					err := p.tx.SetHostPublicKey(p.ctx, contractID, fcr.HostPublicKey())
					if err != nil {
						return fmt.Errorf("r.SetHostPublicKey %s %s: %w", contractID, fcr.HostPublicKey(), err)
					}
				}

				things := make([]domain.TransactionThing, 0,
					len(transaction.SiacoinInputs)+
						len(transaction.SiacoinOutputs)+
						len(transaction.FileContracts)+
						len(transaction.FileContractRevisions)+
						len(transaction.SiafundInputs)+
						len(transaction.SiafundOutputs),
				)
				for j, si := range transaction.SiacoinInputs {
					things = append(things, domain.TransactionThing{
						TransactionID: tx.ID,
						Thing:         domain.ThingCoinInput,
						Index:         j,
						ThingID:       crypto.Hash(si.ParentID),
					})
				}
				for j := range transaction.SiacoinOutputs {
					outputID := transaction.SiacoinOutputID(uint64(j))
					things = append(things, domain.TransactionThing{
						TransactionID: tx.ID,
						Thing:         domain.ThingCoinOutput,
						Index:         j,
						ThingID:       crypto.Hash(outputID),
					})
				}
				for j := range transaction.FileContracts {
					contractID := transaction.FileContractID(uint64(j))
					things = append(things, domain.TransactionThing{
						TransactionID: tx.ID,
						Thing:         domain.ThingContract,
						Index:         j,
						ThingID:       crypto.Hash(contractID),
					})
				}
				for j, cr := range transaction.FileContractRevisions {
					things = append(things, domain.TransactionThing{
						TransactionID: tx.ID,
						Thing:         domain.ThingContractRevision,
						Index:         j,
						ThingID:       crypto.Hash(cr.ParentID),
					})
				}
				for j, si := range transaction.SiafundInputs {
					things = append(things, domain.TransactionThing{
						TransactionID: tx.ID,
						Thing:         domain.ThingFundInput,
						Index:         j,
						ThingID:       crypto.Hash(si.ParentID),
					})
				}
				for j := range transaction.SiafundOutputs {
					outputID := transaction.SiafundOutputID(uint64(j))
					things = append(things, domain.TransactionThing{
						TransactionID: tx.ID,
						Thing:         domain.ThingFundOutput,
						Index:         j,
						ThingID:       crypto.Hash(outputID),
					})
				}
				if len(things) > 0 {
					err = p.tx.AddTransactionThings(p.ctx, things)
					if err != nil {
						return fmt.Errorf("tx.AddTransactionThings blockID=%s transactionID=%s: %w", block.ID, tx.ID, err)
					}
				}
			}

			// Collect info about various identificators block produces.
			bf := makeBlockFacts(appliedBlock, block.Height)

			// Add outputs.
			unaccountedIDs, err := p.processDiffs(block, appliedDiffs, bf)
			if err != nil {
				return fmt.Errorf("p.processDiffs blockID=%s: %w", block.ID, err)
			}

			// Proofs.
			for _, transaction := range appliedBlock.Transactions {
				if len(transaction.StorageProofs) > 0 {
					for _, storageProof := range transaction.StorageProofs {
						proof := domain.StorageProof{
							ContractID:    storageProof.ParentID,
							TransactionID: transaction.ID(),
							BlockID:       block.ID,
							Height:        block.Height,
						}
						err := p.tx.AddStorageProof(p.ctx, proof)
						if err != nil {
							return fmt.Errorf("tx.AddStorageProof blockID=%s contractID=%s: %w", proof.BlockID, proof.ContractID, err)
						}
					}
				}
			}

			// Link addresses from the same wallet.
			err = p.linkAddresses(height, appliedBlock)
			if err != nil {
				return fmt.Errorf("linkAddresses: %w", err)
			}

			// Genesis may create coins and funds.
			if block.Height == 0 {
				unaccountedIDs.fundOutputs = nil
				unaccountedIDs.coinOutputs = nil
			}
			// Airdrop may recall and issue funds.
			if block.Height == types.SpfAirdropHeight {
				unaccountedIDs.fundInputs = nil
				unaccountedIDs.fundOutputs = nil
			}
			// Forks may issue new funds.
			if block.Height == types.SpfHardforkHeight ||
				block.Height == types.SpfSecondHardforkHeight ||
				block.Height == types.Fork2022Height {
				unaccountedIDs.fundOutputs = nil
			}
			// Otherwise there should be no unaccounted things.
			if len(unaccountedIDs.fundInputs) > 0 ||
				len(unaccountedIDs.fundOutputs) > 0 ||
				len(unaccountedIDs.coinInputs) > 0 ||
				len(unaccountedIDs.coinOutputs) > 0 ||
				len(unaccountedIDs.contracts) > 0 {
				p.l.Error("unaccountedIDs",
					zap.Any("fundInputs", unaccountedIDs.fundInputs),
					zap.Any("fundOutputs", unaccountedIDs.fundOutputs),
					zap.Any("coinInputs", unaccountedIDs.coinInputs),
					zap.Any("coinOutputs", unaccountedIDs.coinOutputs),
					zap.Any("contracts", unaccountedIDs.contracts))
				return fmt.Errorf("unaccounted ids")
			}

			// Process transactions ArbitraryData.
			err = p.processArbitrary(block, appliedBlock.Transactions)
			if err != nil {
				return fmt.Errorf("processArbitrary blockID=%s: %w", block.ID, err)
			}
		}

		return nil
	})
	if err != nil {
		debugCC(p.cc)
		return err
	}

	// Finalize and log statistics.
	p.stats.Took = time.Since(started)

	p.l.Info("BlockStats",
		zap.Int("RevertedBlocks", p.stats.RevertedBlocks),

		zap.Int("AddedBlocks", p.stats.AddedBlocks),
		zap.Any("AddedHeightsList", p.stats.AddedHeightsList),
		zap.Int("AddedTransactions", p.stats.AddedTransactions),
		zap.Int("AddedOutputs", p.stats.AddedOutputs),
		zap.Int("UsedInputs", p.stats.UsedInputs),
		zap.Int("AddedContracts", p.stats.AddedContracts),
		zap.Int("RevertedContracts", p.stats.RevertedContracts),
		zap.String("TransferedCoinValue", p.stats.TransferedCoinValue.HumanString()),
		zap.String("TransferedFundaValue", p.stats.TransferedFundaValue.String()),
		zap.String("TransferedFundbValue", p.stats.TransferedFundbValue.String()),
		zap.Int("AddedLinks", p.stats.AddedLinks),

		zap.Duration("took", p.stats.Took))

	return nil
}

func debugCC(cc modules.ConsensusChange) {
	fmt.Printf("ConsensusChange %s\n", cc.ID)

	if len(cc.RevertedBlocks) > 0 {
		fmt.Printf("Reverted blocks:\n")
		for i, block := range cc.RevertedBlocks {
			debugBlock("", block)
			revertedDiffs := cc.RevertedDiffs[i]
			debugDiffs("", revertedDiffs)
		}
	}

	if len(cc.AppliedBlocks) > 0 {
		fmt.Printf("Applied blocks:\n")
		for i, block := range cc.AppliedBlocks {
			debugBlock("", block)
			appliedDiffs := cc.AppliedDiffs[i]
			debugDiffs("", appliedDiffs)
		}
	}
}

func directionAwareIndent(indent string, direction modules.DiffDirection) string {
	if direction == modules.DiffApply {
		return indent + ">>"
	} else {
		return indent + "<<"
	}
}

func debugDiffs(indent string, diffs modules.ConsensusChangeDiffs) {
	if len(diffs.SiacoinOutputDiffs) > 0 {
		fmt.Printf("%s  SiacoinOutputDiffs:\n", indent)
		for _, diff := range diffs.SiacoinOutputDiffs {
			debugCoinOutput(directionAwareIndent(indent+"  ", diff.Direction), diff.ID, diff.SiacoinOutput)
		}
	}
	if len(diffs.FileContractDiffs) > 0 {
		fmt.Printf("%s  FileContractDiffs:\n", indent)
		for _, diff := range diffs.FileContractDiffs {
			debugContract(directionAwareIndent(indent+"  ", diff.Direction), diff.ID, diff.FileContract)
		}
	}
	if len(diffs.SiafundOutputDiffs) > 0 {
		fmt.Printf("%s  SiafundOutputDiffs:\n", indent)
		for _, diff := range diffs.SiafundOutputDiffs {
			debugFundOutput(directionAwareIndent(indent+"  ", diff.Direction), diff.ID, diff.SiafundOutput)
		}
	}
	if len(diffs.DelayedSiacoinOutputDiffs) > 0 {
		fmt.Printf("%s  DelayedSiacoinOutputDiffs:\n", indent)
		for _, diff := range diffs.DelayedSiacoinOutputDiffs {
			fmt.Printf("%s  MaturityHeight: %d\n", indent, diff.MaturityHeight)
			debugCoinOutput(directionAwareIndent(indent+"  ", diff.Direction), diff.ID, diff.SiacoinOutput)
		}
	}
}

func debugBlock(indent string, block types.Block) {
	fmt.Printf("%s- %s\n", indent, block.ID())
	fmt.Printf("%s  Parent: %s\n", indent, block.ParentID)
	if len(block.MinerPayouts) > 0 {
		fmt.Printf("%s  MinerPayouts:\n", indent)
		for i, payout := range block.MinerPayouts {
			debugCoinOutput(indent+"  ", block.MinerPayoutID(uint64(i)), payout)
		}
	}
	if len(block.Transactions) > 0 {
		fmt.Printf("%s  Transactions:\n", indent)
		for _, tx := range block.Transactions {
			debugTx(indent+"  ", tx)
		}
	}
}

func debugCoinOutput(indent string, outputID types.SiacoinOutputID, output types.SiacoinOutput) {
	fmt.Printf("%s- %s\n", indent, outputID)
	fmt.Printf("%s  Value:      %s (%s)\n", indent, output.Value.HumanString(), output.Value)
	fmt.Printf("%s  UnlockHash: %s\n", indent, output.UnlockHash)
}

func debugFundOutput(indent string, outputID types.SiafundOutputID, output types.SiafundOutput) {
	fmt.Printf("%s- %s\n", indent, outputID)
	fmt.Printf("%s  Value:      %s\n", indent, output.Value)
	fmt.Printf("%s  UnlockHash: %s\n", indent, output.UnlockHash)
}

func debugContract(indent string, contractID types.FileContractID, contract types.FileContract) {
	fmt.Printf("%s- %s\n", indent, contractID)
	fmt.Printf("%s  %+v\n", indent, contract)
}

func debugTx(indent string, tx types.Transaction) {
	fmt.Printf("%s- %s\n", indent, tx.ID())
	if len(tx.SiacoinInputs) > 0 {
		fmt.Printf("%s  SiacoinInputs:\n", indent)
		for _, input := range tx.SiacoinInputs {
			fmt.Printf("%s  - %s\n", indent, input.ParentID)
		}
	}
	if len(tx.SiacoinOutputs) > 0 {
		fmt.Printf("%s  SiacoinOutputs:\n", indent)
		for i, output := range tx.SiacoinOutputs {
			debugCoinOutput(indent+"  ", tx.SiacoinOutputID(uint64(i)), output)
		}
	}
	if len(tx.SiafundInputs) > 0 {
		fmt.Printf("%s  SiafundInputs:\n", indent)
		for _, input := range tx.SiafundInputs {
			fmt.Printf("%s  - %s\n", indent, input.ParentID)
		}
	}
	if len(tx.SiafundOutputs) > 0 {
		fmt.Printf("%s  SiafundOutputs:\n", indent)
		for i, output := range tx.SiafundOutputs {
			debugFundOutput(indent+"  ", tx.SiafundOutputID(uint64(i)), output)
		}
	}
	if len(tx.FileContracts) > 0 {
		fmt.Printf("%s  Contracts:\n", indent)
		for i, contract := range tx.FileContracts {
			debugContract(indent+"  ", tx.FileContractID(uint64(i)), contract)
		}
	}
	if len(tx.FileContractRevisions) > 0 {
		fmt.Printf("%s  ContractRevisions:\n", indent)
		for _, contractRevision := range tx.FileContractRevisions {
			fmt.Printf("%s  - %s\n", indent, contractRevision.ID())
			fmt.Printf("%s    %+v\n", indent, contractRevision)
		}
	}
}

func (p *processor) processRevertedBlocks() error {
	p.stats.RevertedBlocks = len(p.cc.RevertedBlocks)

	for _, revertedBlock := range p.cc.RevertedBlocks {
		blockID := revertedBlock.ID()

		err := p.tx.UnuseOutputsInBlock(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.UnuseOutputsInBlock blockID=%s: %w", blockID, err)
		}

		err = p.tx.DeleteArbitraryDataInBlock(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.DeleteArbitraryDataInBlock blockID=%s: %w", blockID, err)
		}

		err = p.tx.DeleteAnnouncementsInBlock(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.DeleteAnnouncementsInBlock blockID=%s: %w", blockID, err)
		}

		err = p.tx.DeleteOutputsInBlock(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.DeleteOutputsInBlock blockID=%s: %w", blockID, err)
		}

		err = p.tx.DeleteContractsInBlock(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.DeleteContractsInBlock blockID=%s: %w", blockID, err)
		}

		err = p.tx.UnrevertContractsRevertedInBlock(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.UnrevertContractsRevertedInBlock blockID=%s: %w", blockID, err)
		}

		err = p.tx.DeleteTransactionThings(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.DeleteTransactionThings blockID=%s: %w", blockID, err)
		}

		err = p.tx.DeleteLinksInBlock(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.DeleteLinksInBlock blockID=%s: %w", blockID, err)
		}

		err = p.tx.DeleteRootsInBlock(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.DeleteRootsInBlock blockID=%s: %w", blockID, err)
		}

		err = p.tx.DeleteTransactionsInBlock(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.DeleteTransactionsInBlock blockID=%s: %w", blockID, err)
		}

		err = p.tx.DeleteBlock(p.ctx, blockID)
		if err != nil {
			return fmt.Errorf("tx.DeleteBlock blockID=%s: %w", blockID, err)
		}
	}

	return nil
}

type unaccountedDiffs struct {
	coinInputs  []types.SiacoinOutputID
	coinOutputs []types.SiacoinOutputID
	fundInputs  []types.SiafundOutputID
	fundOutputs []types.SiafundOutputID
	contracts   []types.FileContractID
}

func (p *processor) processDiffs(block domain.Block, appliedDiffs modules.ConsensusChangeDiffs, bf *blockFacts) (unaccountedDiffs, error) { //nolint:gocyclo
	var un unaccountedDiffs

	proofOutputs := make(map[crypto.Hash]domain.Output)
	for _, diff := range appliedDiffs.FileContractDiffs {
		contractID := diff.ID

		if diff.Direction == modules.DiffApply {
			transactionID := bf.consumeTransactionContract(contractID)
			if transactionID == nil {
				un.contracts = append(un.contracts, contractID)
			}

			inputsMap := make(map[types.OutputID]types.SiacoinOutput)
			inputIDs := bf.transactionCoinInputs(*transactionID)
			if len(inputIDs) > 0 {
				// inputs created in this block?
				var inputIDs2 []types.OutputID
				for _, id := range inputIDs {
					if so, ok := bf.outputByID(id); ok {
						inputsMap[id] = so
					} else {
						inputIDs2 = append(inputIDs2, id)
					}
				}
				if len(inputIDs2) > 0 {
					var err error
					inputs2, err := p.tx.OutputsByIDs(p.ctx, inputIDs2)
					if err != nil {
						return un, fmt.Errorf("tx.OutputsByIDs: %w", err)
					}
					if len(inputs2) != len(inputIDs2) {
						ids := make(map[types.OutputID]struct{})
						for _, id := range inputIDs {
							ids[id] = struct{}{}
						}
						for _, inp := range inputs2 {
							delete(ids, inp.ID)
						}
						var missingIDs []string
						for id := range ids {
							missingIDs = append(missingIDs, id.String())
						}
						return un, fmt.Errorf("could not find output ids: " + strings.Join(missingIDs, ", "))
					}
					for _, inp := range inputs2 {
						inputsMap[inp.ID] = types.SiacoinOutput{
							Value:      inp.CoinValue,
							UnlockHash: inp.UnlockHash,
						}
					}
				}
			}

			inputs := make([]types.SiacoinOutput, len(inputIDs))
			for i, inputID := range inputIDs {
				inputs[i] = inputsMap[inputID]
			}

			outputs := bf.transactionCoinOutputs(*transactionID)

			hostCollateral, renterAllowance := contractFunding(diff.FileContract, inputs, outputs)

			contract := domain.Contract{
				ID:          contractID,
				Revision:    diff.FileContract.RevisionNumber,
				Filesize:    diff.FileContract.FileSize,
				WindowStart: diff.FileContract.WindowStart,
				WindowEnd:   diff.FileContract.WindowEnd,
				Payout:      diff.FileContract.Payout,
				UnlockHash:  diff.FileContract.UnlockHash,

				BlockID:       block.ID,
				Height:        block.Height,
				TransactionID: transactionID,

				ValidRenterPayout:    diff.FileContract.ValidRenterOutput().Value,
				ValidRenterAddress:   diff.FileContract.ValidRenterOutput().UnlockHash,
				ValidRenterOutputID:  contractID.StorageProofOutputID(types.ProofValid, domain.ProofIdxValidRenter),
				ValidHostPayout:      diff.FileContract.ValidHostOutput().Value,
				ValidHostAddress:     diff.FileContract.ValidHostOutput().UnlockHash,
				ValidHostOutputID:    contractID.StorageProofOutputID(types.ProofValid, domain.ProofIdxValidHost),
				MissedRenterPayout:   diff.FileContract.MissedRenterOutput().Value,
				MissedRenterAddress:  diff.FileContract.MissedRenterOutput().UnlockHash,
				MissedRenterOutputID: contractID.StorageProofOutputID(types.ProofMissed, domain.ProofIdxMissedRenter),
				MissedHostPayout:     diff.FileContract.MissedHostOutput().Value,
				MissedHostAddress:    diff.FileContract.MissedHostOutput().UnlockHash,
				MissedHostOutputID:   contractID.StorageProofOutputID(types.ProofMissed, domain.ProofIdxMissedHost),
				MissedVoidPayout:     types.ZeroCurrency,
				MissedVoidAddress:    types.BurnAddressUnlockHash,
				MissedVoidOutputID:   contractID.StorageProofOutputID(types.ProofMissed, domain.ProofIdxMissedVoid),

				HostCollateral:  hostCollateral,
				RenterAllowance: renterAllowance,
			}
			missedVoidOutput, err := diff.FileContract.MissedVoidOutput()
			if err != nil {
				contract.MissedVoidPayout = missedVoidOutput.Value
				contract.MissedVoidAddress = missedVoidOutput.UnlockHash
			}

			err = p.tx.AddContract(p.ctx, contract)
			if err != nil {
				return un, fmt.Errorf("tx.AddContract contractID=%s: %w", contractID, err)
			}

			p.stats.AddedContracts++
		} else {
			err := p.tx.RevertContract(p.ctx, contractID, diff.FileContract.RevisionNumber, block.ID, block.Height)
			if err != nil {
				return un, fmt.Errorf("tx.RevertContract contractID=%s: %w", contractID, err)
			}

			p.stats.RevertedContracts++

			// Contract things.
			for i := range diff.FileContract.ValidProofOutputs {
				outputID := contractID.StorageProofOutputID(types.ProofValid, uint64(i))
				output := domain.Output{
					ID:               types.OutputID(outputID),
					Type:             domain.ValidProofType(i),
					OutputBlockID:    block.ID,
					OutputHeight:     block.Height,
					ContractID:       &contractID,
					ContractRevision: diff.FileContract.RevisionNumber,
				}
				proofOutputs[crypto.Hash(outputID)] = output
			}
			for i := range diff.FileContract.MissedProofOutputs {
				outputID := contractID.StorageProofOutputID(types.ProofMissed, uint64(i))
				output := domain.Output{
					ID:               types.OutputID(outputID),
					Type:             domain.MissedProofType(i),
					OutputBlockID:    block.ID,
					OutputHeight:     block.Height,
					ContractID:       &contractID,
					ContractRevision: diff.FileContract.RevisionNumber,
				}
				proofOutputs[crypto.Hash(outputID)] = output
			}
		}
	}

	// Make map of applied coin outputs: outputID => SiacoinOutputs.
	appliedCoinOutputs := make(map[crypto.Hash]types.SiacoinOutput)
	for _, diff := range appliedDiffs.SiacoinOutputDiffs {
		if diff.Direction == modules.DiffApply {
			appliedCoinOutputs[crypto.Hash(diff.ID)] = diff.SiacoinOutput
		}
	}

	// Collect matured coin outputs. We do nothing in database, but in
	// blockchain there should be two diffs: revert of a DelayedSiacoinOutput,
	// and apply of exact SiacoinOutput.
	maturedCoinOutputs := make(map[crypto.Hash]struct{})
	for _, diff := range appliedDiffs.DelayedSiacoinOutputDiffs {
		outputID := diff.ID

		if diff.Direction == modules.DiffApply {
			output, ok := proofOutputs[crypto.Hash(outputID)]
			if ok {
				output.UnlockHash = diff.SiacoinOutput.UnlockHash
				output.CoinValue = diff.SiacoinOutput.Value
			} else {
				output = domain.Output{
					ID:             types.OutputID(outputID),
					Type:           "",
					UnlockHash:     diff.SiacoinOutput.UnlockHash,
					CoinValue:      diff.SiacoinOutput.Value,
					MaturityHeight: diff.MaturityHeight,

					OutputBlockID: block.ID,
					OutputHeight:  block.Height,
				}

				// Add fields: Type, OutputTransactionID, ContractID,
				// ContractRevision (where applicable).
				output, ok = bf.consumeUpdateCoinOutput(output)
				if !ok {
					un.coinOutputs = append(un.coinOutputs, outputID)
				}
			}

			err := p.tx.AddOutput(p.ctx, output)
			if err != nil {
				return un, fmt.Errorf("tx.AddOutput outputID=%s: %w", outputID, err)
			}

			p.stats.AddedOutputs++
			p.stats.TransferedCoinValue = p.stats.TransferedCoinValue.Add(output.CoinValue)
		} else {
			// Matured output?
			if diff.MaturityHeight == block.Height {
				appliedCoinOutput, ok := appliedCoinOutputs[crypto.Hash(outputID)]
				if ok && appliedCoinOutput.Value.Equals(diff.SiacoinOutput.Value) &&
					appliedCoinOutput.UnlockHash == diff.SiacoinOutput.UnlockHash {
					maturedCoinOutputs[crypto.Hash(outputID)] = struct{}{}
					continue
				}
			}

			transactionID := bf.consumeTransactionCoinInput(outputID)
			if transactionID == nil {
				un.coinInputs = append(un.coinInputs, outputID)
			}

			_, err := p.tx.UseOutput(p.ctx, types.OutputID(outputID), block.ID, block.Height, transactionID)
			if err != nil {
				return un, fmt.Errorf("tx.UseOutput outputID=%s: %w", outputID, err)
			}

			p.stats.UsedInputs++
		}
	}

	for _, diff := range appliedDiffs.SiacoinOutputDiffs {
		outputID := diff.ID

		if diff.Direction == modules.DiffApply {
			if _, ok := maturedCoinOutputs[crypto.Hash(outputID)]; ok {
				_, err := p.tx.OutputByID(p.ctx, types.OutputID(outputID))
				if err != nil {
					if errors.Is(err, domain.ErrNotFound) {
						if block.Height == 144 {
							// Sometimes (i.e. block height 144) output may mature out
							// of nowhere. So we add output here just in case. But usually
							// they are already in database.
							output := domain.Output{
								ID:         types.OutputID(outputID),
								Type:       "",
								UnlockHash: diff.SiacoinOutput.UnlockHash,
								CoinValue:  diff.SiacoinOutput.Value,

								OutputBlockID: block.ID,
								OutputHeight:  block.Height,
							}
							err := p.tx.AddOutput(p.ctx, output)
							if err != nil {
								return un, fmt.Errorf("tx.AddOutput outputID=%s: %w", outputID, err)
							}

							p.stats.AddedOutputs++
							p.stats.TransferedCoinValue = p.stats.TransferedCoinValue.Add(output.CoinValue)
						}
					} else {
						return un, fmt.Errorf("output not found: %s", outputID)
					}
				}

				// Update balance.
				change := domain.Balance{
					Address:     diff.SiacoinOutput.UnlockHash,
					CoinBalance: diff.SiacoinOutput.Value,
				}
				err = p.tx.IncrBalance(p.ctx, change)
				if err != nil {
					return un, fmt.Errorf("tx.IncrBalance address=%s: %w", change.Address, err)
				}
				continue
			}

			output := domain.Output{
				ID:         types.OutputID(outputID),
				Type:       "",
				UnlockHash: diff.SiacoinOutput.UnlockHash,
				CoinValue:  diff.SiacoinOutput.Value,

				OutputBlockID: block.ID,
				OutputHeight:  block.Height,
			}

			// Add fields: Type, OutputTransactionID, ContractID,
			// ContractRevision (where applicable).
			output, ok := bf.consumeUpdateCoinOutput(output)
			if !ok {
				un.coinOutputs = append(un.coinOutputs, outputID)
			}

			err := p.tx.AddOutput(p.ctx, output)
			if err != nil {
				return un, fmt.Errorf("tx.AddOutput outputID=%s: %w", outputID, err)
			}

			// Update balance.
			change := domain.Balance{
				Address:     diff.SiacoinOutput.UnlockHash,
				CoinBalance: diff.SiacoinOutput.Value,
			}
			err = p.tx.IncrBalance(p.ctx, change)
			if err != nil {
				return un, fmt.Errorf("tx.IncrBalance address=%s: %w", change.Address, err)
			}

			// Stats.
			p.stats.AddedOutputs++
			p.stats.TransferedCoinValue = p.stats.TransferedCoinValue.Add(output.CoinValue)
		} else {
			transactionID := bf.consumeTransactionCoinInput(outputID)
			if transactionID == nil {
				un.coinInputs = append(un.coinInputs, outputID)
			}

			input, err := p.tx.UseOutput(p.ctx, types.OutputID(outputID), block.ID, block.Height, transactionID)
			if err != nil {
				return un, fmt.Errorf("tx.UseOutput outputID=%s: %w", outputID, err)
			}

			// Update balance.
			change := domain.Balance{
				Address:     diff.SiacoinOutput.UnlockHash,
				CoinBalance: input.CoinValue, // Sometimes inputs have 0 value (forks?), so we get them from database, not from blockchain.
			}
			err = p.tx.DecrBalance(p.ctx, change)
			if err != nil {
				return un, fmt.Errorf("tx.DecrBalance address=%s: %w", change.Address, err)
			}

			// Stats.
			p.stats.UsedInputs++
		}
	}

	fundbTransactions := make(map[crypto.Hash]struct{})
	for _, diff := range appliedDiffs.SiafundOutputDiffs {
		outputID := diff.ID

		if diff.Direction == modules.DiffApply {
			transactionID := bf.consumeTransactionFundOutput(outputID)
			if transactionID == nil {
				un.fundOutputs = append(un.fundOutputs, outputID)
			}

			output := domain.Output{
				ID:         types.OutputID(outputID),
				Type:       "",
				UnlockHash: diff.SiafundOutput.UnlockHash,

				OutputBlockID:       block.ID,
				OutputHeight:        block.Height,
				OutputTransactionID: transactionID,
			}

			fundb := false
			if transactionID != nil {
				_, fundb = fundbTransactions[crypto.Hash(*transactionID)]
			}
			if fundb || (block.Height == types.Fork2022Height && diff.SiafundOutput.Value.Equals64(200000000)) {
				output.FundbValue = diff.SiafundOutput.Value
			} else {
				output.FundaValue = diff.SiafundOutput.Value
			}

			err := p.tx.AddOutput(p.ctx, output)
			if err != nil {
				return un, fmt.Errorf("tx.AddOutput outputID=%s: %w", outputID, err)
			}

			// Update balance.
			change := domain.Balance{
				Address:      diff.SiafundOutput.UnlockHash,
				FundaBalance: output.FundaValue,
				FundbBalance: output.FundbValue,
			}
			err = p.tx.IncrBalance(p.ctx, change)
			if err != nil {
				return un, fmt.Errorf("tx.IncrBalance address=%s: %w", change.Address, err)
			}

			// Stats.
			p.stats.AddedOutputs++
			p.stats.TransferedFundaValue = p.stats.TransferedFundaValue.Add(output.FundaValue)
			p.stats.TransferedFundbValue = p.stats.TransferedFundbValue.Add(output.FundbValue)
		} else {
			transactionID := bf.consumeTransactionFundInput(outputID)
			if transactionID == nil {
				un.fundInputs = append(un.fundInputs, outputID)
			}
			input, err := p.tx.UseOutput(p.ctx, types.OutputID(outputID), block.ID, block.Height, transactionID)
			if err != nil {
				return un, fmt.Errorf("tx.UseOutput outputID=%s: %w", outputID, err)
			}
			if transactionID != nil && !input.FundbValue.IsZero() {
				fundbTransactions[crypto.Hash(*transactionID)] = struct{}{}
			}

			// Update balance.
			change := domain.Balance{
				Address:      diff.SiafundOutput.UnlockHash,
				FundaBalance: input.FundaValue,
				FundbBalance: input.FundbValue,
			}
			err = p.tx.DecrBalance(p.ctx, change)
			if err != nil {
				return un, fmt.Errorf("tx.DecrBalance address=%s: %w", change.Address, err)
			}

			p.stats.UsedInputs++
		}
	}

	return un, nil
}

func (p *processor) linkAddresses(height types.BlockHeight, block types.Block) error {
	// Collect all inputs.
	blockInputIDs := make(map[types.OutputID]struct{})
	for _, transaction := range block.Transactions {
		for _, si := range transaction.SiacoinInputs {
			blockInputIDs[types.OutputID(si.ParentID)] = struct{}{}
		}
	}

	for _, transaction := range block.Transactions {
		// Setup transaction: unlock hashes of inputs and outputs belong
		// to the same wallet.
		if modules.TransactionType(&transaction) == modules.TXTypeSetup &&
			len(transaction.SiacoinInputs) >= 1 {
			// Track links between unlock hash of the first input (mainAddress,
			// defined below) and unlock hashes of other inputs and outputs.
			var firstInputAddr types.UnlockHash
			linksMap := make(map[types.UnlockHash]struct{})

			// All the inputs are from same wallet.
			inputIDs := make([]types.OutputID, 0, len(transaction.SiacoinInputs))
			for _, si := range transaction.SiacoinInputs {
				inputIDs = append(inputIDs, types.OutputID(si.ParentID))
			}
			outputs, err := p.tx.OutputsByIDs(p.ctx, inputIDs)
			if err != nil {
				return fmt.Errorf("tx.OutputsByIDs: %w", err)
			}
			if len(outputs) != len(transaction.SiacoinInputs) {
				return fmt.Errorf("expected %d outputs, but got %d", len(transaction.SiacoinInputs), len(outputs))
			}
			firstInputAddr = outputs[0].UnlockHash
			for _, o := range outputs {
				linksMap[o.UnlockHash] = struct{}{}
			}

			// All outputs belong to the same wallet.
			for _, o := range transaction.SiacoinOutputs {
				linksMap[o.UnlockHash] = struct{}{}
			}

			var links []domain.Link
			for addr := range linksMap {
				if addr != firstInputAddr {
					links = append(links, domain.Link{
						Address1:      firstInputAddr,
						Address2:      addr,
						Type:          domain.LinkTypeSetup,
						TransactionID: transaction.ID(),
						Height:        height,
					})
				}
			}
			if len(links) > 0 {
				err = p.addLinks(links)
				if err != nil {
					return fmt.Errorf("addLinks1: %w", err)
				}
				p.stats.AddedLinks += len(links)
				p.l.Info("linked setup tx",
					zap.Stringer("tx", transaction.ID()),
					zap.Int("num", len(links)))
			}
		}

		// Announcements: unlock hashes of inputs used to pay for announcements
		// of same pubkey belong to the same wallet.
		for _, value := range transaction.ArbitraryData {
			_, pubKey, err := modules.DecodeAnnouncement(value)
			if err != nil {
				continue
			}
			inputID1 := types.OutputID(transaction.SiacoinInputs[0].ParentID)
			input1, err := p.tx.OutputByID(p.ctx, inputID1)
			if err != nil {
				return fmt.Errorf("tx.OutputByID %s: %w", inputID1, err)
			}

			// Find another announcement.
			ann, err := p.tx.EarliestAnnouncement(p.ctx, pubKey)
			if err != nil {
				if errors.Is(err, domain.ErrNotFound) {
					continue
				}
				return fmt.Errorf("tx.EarliestAnnouncement: %w", err)
			}
			tts, err := p.tx.GetTransactionThings(p.ctx, ann.TransactionID)
			if err != nil {
				return fmt.Errorf("tx.GetTransactionThings: %w", err)
			}
			// Announcement transaction should have exactly one input and
			// no outputs.
			inputsNum := 0
			var inputID2 types.OutputID
			for _, tt := range tts {
				if tt.Thing == domain.ThingCoinOutput ||
					tt.Thing == domain.ThingContract ||
					tt.Thing == domain.ThingContractRevision ||
					tt.Thing == domain.ThingFundInput ||
					tt.Thing == domain.ThingFundOutput {
					return fmt.Errorf("bad announcement tx: %s", ann.TransactionID)
				}
				inputsNum++
				if inputsNum > 1 {
					return fmt.Errorf("bad announcement tx: %s", ann.TransactionID)
				}
				inputID2 = types.OutputID(tt.ThingID)
			}
			input2, err := p.tx.OutputByID(p.ctx, inputID2)
			if err != nil {
				return fmt.Errorf("tx.OutputByID %s: %w", inputID2, err)
			}

			if input1.UnlockHash != input2.UnlockHash {
				// We are using earlier unlock hash as a first address.
				addr1 := input2.UnlockHash
				addr2 := input1.UnlockHash
				err = p.addLinks([]domain.Link{
					{
						Address1:      addr1,
						Address2:      addr2,
						Type:          domain.LinkTypeAnnouncement,
						TransactionID: transaction.ID(),
						Height:        height,
					},
				})
				if err != nil {
					return fmt.Errorf("addLinks2: %w", err)
				}
				p.stats.AddedLinks += 1
				p.l.Info("linked announcement tx",
					zap.Stringer("tx", transaction.ID()),
					zap.Stringer("a1", addr1),
					zap.Stringer("a2", addr2))
			}
		}

		// Defrag transactions.
		if len(transaction.SiacoinInputs) == defragBatchSize && len(transaction.SiacoinOutputs) == 1 {
			// Using same technique as in setup tx.
			var firstInputAddr types.UnlockHash
			linksMap := make(map[types.UnlockHash]struct{})

			inputIDs := make([]types.OutputID, 0, len(transaction.SiacoinInputs))
			for _, si := range transaction.SiacoinInputs {
				inputIDs = append(inputIDs, types.OutputID(si.ParentID))
			}
			outputs, err := p.tx.OutputsByIDs(p.ctx, inputIDs)
			if err != nil {
				return fmt.Errorf("tx.OutputsByIDs: %w", err)
			}
			firstInputAddr = outputs[0].UnlockHash
			for _, o := range outputs {
				linksMap[o.UnlockHash] = struct{}{}
			}

			outputAddr := transaction.SiacoinOutputs[0].UnlockHash
			linksMap[outputAddr] = struct{}{}

			var links []domain.Link
			for addr := range linksMap {
				if addr != firstInputAddr {
					links = append(links, domain.Link{
						Address1:      firstInputAddr,
						Address2:      addr,
						Type:          domain.LinkTypeDefrag,
						TransactionID: transaction.ID(),
						Height:        height,
					})
				}
			}
			if len(links) > 0 {
				err = p.addLinks(links)
				if err != nil {
					return fmt.Errorf("addLinks3: %w", err)
				}
				p.stats.AddedLinks += len(links)
				p.l.Info("linked defrag tx",
					zap.Stringer("tx", transaction.ID()),
					zap.Int("num", len(links)))
			}
		}
	}

	return nil
}

func (p *processor) addLinks(links []domain.Link) error {
	err := p.tx.AddLinks(p.ctx, links)
	if err != nil {
		return fmt.Errorf("tx.AddLinks: %w", err)
	}

	var addresses []types.UnlockHash
	uniqAddr := make(map[types.UnlockHash]struct{})
	for _, ln := range links {
		if _, ok := uniqAddr[ln.Address1]; !ok {
			addresses = append(addresses, ln.Address1)
			uniqAddr[ln.Address1] = struct{}{}
		}
		if _, ok := uniqAddr[ln.Address2]; !ok {
			addresses = append(addresses, ln.Address2)
			uniqAddr[ln.Address2] = struct{}{}
		}
	}

	rtm, err := p.tx.FindRoots(p.ctx, addresses)
	if err != nil {
		return fmt.Errorf("tx.FindRoots: %w", err)
	}

	newRoots := make([]domain.Root, 0)
	for _, ln := range links { // (a1, a2)
		rt1, rt1ok := rtm[ln.Address1] // a1 -> r1
		rt2, rt2ok := rtm[ln.Address2] // a2 -> r2
		switch {
		case rt1ok && rt2ok:
			if rt1.Root == rt2.Root {
				continue
			}
			// Link two roots.
			// Choose root with the earliest height as new root,
			// and other root as address.
			// Choose the earliest address as root.
			h1, err := p.tx.UnlockHashOutputHeight(p.ctx, rt1.Root)
			if err != nil {
				return fmt.Errorf("tx.UnlockHashOutputHeight: %w", err)
			}

			h2, err := p.tx.UnlockHashOutputHeight(p.ctx, rt2.Root)
			if err != nil {
				return fmt.Errorf("tx.UnlockHashOutputHeight: %w", err)
			}

			var oldRoot, newRoot types.UnlockHash
			if h1 <= h2 {
				// update r2 to r1 in a2->r2 and r2->r2
				oldRoot, newRoot = rt2.Root, rt1.Root
			} else {
				// update r1 to r2 in a1->r1 and r1->r1
				oldRoot, newRoot = rt1.Root, rt2.Root
			}
			err = p.tx.UpdateRootAddress(p.ctx, oldRoot, newRoot)
			if err != nil {
				return fmt.Errorf("tx.UpdateRootAddress: %w", err)
			}
			for a, rt := range rtm {
				if rt.Root == oldRoot {
					rt.Root = newRoot
					rtm[a] = rt
				}
			}
			for i, rt := range newRoots {
				if rt.Root == oldRoot {
					newRoots[i].Root = newRoot
				}
			}
		case rt1ok:
			// add a2 -> r1
			add := domain.Root{
				Address:       ln.Address2,
				Root:          rt1.Root,
				Type:          ln.Type,
				TransactionID: ln.TransactionID,
				Height:        ln.Height,
			}
			rtm[add.Address] = add
			newRoots = append(newRoots, add)
		case rt2ok:
			// add a1 -> r2
			add := domain.Root{
				Address:       ln.Address1,
				Root:          rt2.Root,
				Type:          ln.Type,
				TransactionID: ln.TransactionID,
				Height:        ln.Height,
			}
			rtm[add.Address] = add
			newRoots = append(newRoots, add)
		default:
			// Choose the earliest address as root.
			h1, err := p.tx.UnlockHashOutputHeight(p.ctx, ln.Address1)
			if err != nil {
				return fmt.Errorf("tx.UnlockHashOutputHeight: %w", err)
			}

			h2, err := p.tx.UnlockHashOutputHeight(p.ctx, ln.Address2)
			if err != nil {
				return fmt.Errorf("tx.UnlockHashOutputHeight: %w", err)
			}

			var root, addr = ln.Address1, ln.Address2
			if h2 < h1 {
				root, addr = ln.Address2, ln.Address1
			}
			add1 := domain.Root{
				Address:       addr,
				Root:          root,
				Type:          ln.Type,
				TransactionID: ln.TransactionID,
				Height:        ln.Height,
			}
			// Add convenient root->root link.
			add2 := domain.Root{
				Address:       root,
				Root:          root,
				Type:          ln.Type,
				TransactionID: ln.TransactionID,
				Height:        ln.Height,
			}
			rtm[add1.Address] = add1
			rtm[add2.Address] = add2
			newRoots = append(newRoots, add1, add2)
		}
	}

	if len(newRoots) > 0 {
		err = p.tx.AddRoots(p.ctx, newRoots)
		if err != nil {
			return fmt.Errorf("tx.AddRoots: %w", err)
		}
	}

	return nil
}

func (p *processor) processArbitrary(block domain.Block, transactions []types.Transaction) error {
	for _, transaction := range transactions {
		if len(transaction.ArbitraryData) > 0 {
			arbitraryData := domain.ArbitraryData{
				BlockID:       block.ID,
				TransactionID: transaction.ID(),
				Data:          transaction.ArbitraryData,
			}
			err := p.tx.AddArbitraryData(p.ctx, arbitraryData)
			if err != nil {
				return fmt.Errorf("r.AddArbitraryData blockID=%s transactionID=%s: %w", arbitraryData.BlockID, arbitraryData.TransactionID, err)
			}

			for j, value := range transaction.ArbitraryData {
				addr, pubKey, err := modules.DecodeAnnouncement(value)
				if err != nil {
					continue
				}
				announcement := domain.Announcement{
					PublicKey:     pubKey,
					TransactionID: transaction.ID(),
					DataIndex:     j,
					BlockID:       block.ID,
					Height:        block.Height,
					Address:       addr,
				}
				err = p.tx.AddAnnouncement(p.ctx, announcement)
				if err != nil {
					return fmt.Errorf("r.AddAnnouncement blockID=%s transactionID=%s: %w", announcement.BlockID, announcement.TransactionID, err)
				}
			}
		}
	}

	return nil
}

func contractFunding(fc types.FileContract, inputs, outputs []types.SiacoinOutput) (hostCollateral, renterAllowance types.Currency) {
	if len(inputs) == 0 && len(outputs) == 0 {
		// contract revision
		return types.ZeroCurrency, types.ZeroCurrency
	}

	// inputs minus outputs per unlock hash
	// can be negative
	inputBalances := make(map[types.UnlockHash]types.Currency)
	outputBalances := make(map[types.UnlockHash]types.Currency)
	total := types.NewCurrency64(0)
	for _, inp := range inputs {
		if _, ok := inputBalances[inp.UnlockHash]; !ok {
			inputBalances[inp.UnlockHash] = types.NewCurrency64(0)
		}
		inputBalances[inp.UnlockHash] = inputBalances[inp.UnlockHash].Add(inp.Value)
		outputBalances[inp.UnlockHash] = types.NewCurrency64(0)
		total = total.Add(inp.Value)
	}
	for _, out := range outputs {
		if _, ok := outputBalances[out.UnlockHash]; !ok {
			outputBalances[out.UnlockHash] = types.NewCurrency64(0)
		}
		outputBalances[out.UnlockHash] = outputBalances[out.UnlockHash].Add(out.Value)
		total = total.Sub(out.Value)
	}

	// 1. Output(s) where unlock_hash matches with ValidHostOutput address
	hostAddress := fc.ValidHostOutput().UnlockHash
	if _, ok := inputBalances[hostAddress]; ok {
		hc := inputBalances[hostAddress].Sub(outputBalances[hostAddress])
		ra := total.Sub(hc)
		if hc.Cmp(types.ZeroCurrency) >= 0 && ra.Cmp(types.ZeroCurrency) >= 0 {
			return hc, ra
		}
	}

	// 2. One unlock hash - no collateral, everything is renter's.
	if len(inputBalances) == 1 {
		return types.NewCurrency64(0), total
	}

	// 3. Last input is usually host collateral.
	hc := inputs[len(inputs)-1].Value
	ra := total.Sub(hc)
	return hc, ra
}
