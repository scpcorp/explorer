package syncer

import (
	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
)

type blockFacts struct {
	coinInputs  map[crypto.Hash]types.TransactionID
	coinOutputs map[crypto.Hash]blockFactsOutput
	fundInputs  map[crypto.Hash]types.TransactionID
	fundOutputs map[crypto.Hash]types.TransactionID
	contracts   map[crypto.Hash][]types.TransactionID
	txInputs    map[types.TransactionID][]types.OutputID
	txOutputs   map[types.TransactionID][]types.SiacoinOutput
	outputs     map[types.OutputID]types.SiacoinOutput
}

const (
	TypeMinerPayout = iota
	TypeTransactionOutput
	TypeContractValid
	TypeContractMissed
	TypeContractRevisionValid
	TypeContractRevisionMissed
	TypeClaim
	TypeDevClaim
)

type blockFactsOutput struct {
	Type             int
	TransactionID    types.TransactionID
	ContractID       types.FileContractID
	ContractRevision uint64
	Fundb            bool
	OutputNum        int
}

func makeBlockFacts(b types.Block, height types.BlockHeight) *blockFacts {
	bi := &blockFacts{
		coinInputs:  make(map[crypto.Hash]types.TransactionID),
		coinOutputs: make(map[crypto.Hash]blockFactsOutput),
		fundInputs:  make(map[crypto.Hash]types.TransactionID),
		fundOutputs: make(map[crypto.Hash]types.TransactionID),
		contracts:   make(map[crypto.Hash][]types.TransactionID),
		txInputs:    make(map[types.TransactionID][]types.OutputID),
		txOutputs:   make(map[types.TransactionID][]types.SiacoinOutput),
		outputs:     make(map[types.OutputID]types.SiacoinOutput),
	}

	for i := range b.MinerPayouts {
		outputID := b.MinerPayoutID(uint64(i))
		bi.coinOutputs[crypto.Hash(outputID)] = blockFactsOutput{
			Type: TypeMinerPayout,
		}
	}

	for _, transaction := range b.Transactions {
		transactionID := transaction.ID()

		for _, coinInput := range transaction.SiacoinInputs {
			bi.coinInputs[crypto.Hash(coinInput.ParentID)] = transactionID
			bi.txInputs[transactionID] = append(bi.txInputs[transactionID], types.OutputID(coinInput.ParentID))
		}

		for i, so := range transaction.SiacoinOutputs {
			outputID := transaction.SiacoinOutputID(uint64(i))
			bi.coinOutputs[crypto.Hash(outputID)] = blockFactsOutput{
				Type:          TypeTransactionOutput,
				TransactionID: transactionID,
			}
			bi.txOutputs[transactionID] = append(bi.txOutputs[transactionID], so)
			bi.outputs[types.OutputID(outputID)] = so
		}

		for _, fundInput := range transaction.SiafundInputs {
			bi.fundInputs[crypto.Hash(fundInput.ParentID)] = transactionID

			claimID := fundInput.ParentID.SiaClaimOutputID()
			bi.coinOutputs[crypto.Hash(claimID)] = blockFactsOutput{
				Type:          TypeClaim,
				TransactionID: transactionID,
			}

			devClaimID := fundInput.ParentID.SiaClaimSecondOutputID()
			bi.coinOutputs[crypto.Hash(devClaimID)] = blockFactsOutput{
				Type:          TypeDevClaim,
				TransactionID: transactionID,
			}
		}

		for i := range transaction.SiafundOutputs {
			outputID := transaction.SiafundOutputID(uint64(i))
			bi.fundOutputs[crypto.Hash(outputID)] = transactionID
		}

		for i, contract := range transaction.FileContracts {
			contractID := transaction.FileContractID(uint64(i))
			bi.contracts[crypto.Hash(contractID)] = append(bi.contracts[crypto.Hash(contractID)], transactionID)

			for j := range contract.ValidProofOutputs {
				outputID := contractID.StorageProofOutputID(types.ProofValid, uint64(j))
				bi.coinOutputs[crypto.Hash(outputID)] = blockFactsOutput{
					Type:             TypeContractValid,
					OutputNum:        j,
					TransactionID:    transactionID,
					ContractID:       contractID,
					ContractRevision: contract.RevisionNumber,
				}
			}

			for j := range contract.MissedProofOutputs {
				outputID := contractID.StorageProofOutputID(types.ProofMissed, uint64(j))
				bi.coinOutputs[crypto.Hash(outputID)] = blockFactsOutput{
					Type:             TypeContractMissed,
					OutputNum:        j,
					TransactionID:    transactionID,
					ContractID:       contractID,
					ContractRevision: contract.RevisionNumber,
				}
			}
		}

		for _, contractRevision := range transaction.FileContractRevisions {
			contractID := contractRevision.ParentID
			bi.contracts[crypto.Hash(contractID)] = append(bi.contracts[crypto.Hash(contractID)], transactionID)

			for j := range contractRevision.NewValidProofOutputs {
				outputID := contractID.StorageProofOutputID(types.ProofValid, uint64(j))
				bi.coinOutputs[crypto.Hash(outputID)] = blockFactsOutput{
					Type:             TypeContractRevisionValid,
					OutputNum:        j,
					TransactionID:    transactionID,
					ContractID:       contractID,
					ContractRevision: contractRevision.NewRevisionNumber,
				}
			}

			for j := range contractRevision.NewMissedProofOutputs {
				outputID := contractID.StorageProofOutputID(types.ProofMissed, uint64(j))
				bi.coinOutputs[crypto.Hash(outputID)] = blockFactsOutput{
					Type:             TypeContractRevisionMissed,
					OutputNum:        j,
					TransactionID:    transactionID,
					ContractID:       contractID,
					ContractRevision: contractRevision.NewRevisionNumber,
				}
			}
		}
	}

	return bi
}

func (bf *blockFacts) consumeTransactionCoinInput(outputID types.SiacoinOutputID) *types.TransactionID {
	key := crypto.Hash(outputID)
	transactionID, ok := bf.coinInputs[key]
	if ok {
		delete(bf.coinInputs, key)
		return &transactionID
	}
	return nil
}

func (bf *blockFacts) consumeUpdateCoinOutput(output domain.Output) (domain.Output, bool) {
	key := crypto.Hash(output.ID)
	i, ok := bf.coinOutputs[key]
	if !ok {
		return output, false
	}
	delete(bf.coinOutputs, key)

	switch i.Type {
	case TypeMinerPayout:
		output.Type = domain.OutputTypeMinerPayout
	case TypeTransactionOutput:
		output.OutputTransactionID = &i.TransactionID
	case TypeContractValid, TypeContractRevisionValid:
		output.Type = domain.ValidProofType(i.OutputNum)
		output.OutputTransactionID = &i.TransactionID
		output.ContractID = &i.ContractID
		output.ContractRevision = i.ContractRevision
	case TypeContractMissed, TypeContractRevisionMissed:
		output.Type = domain.MissedProofType(i.OutputNum)
		output.OutputTransactionID = &i.TransactionID
		output.ContractID = &i.ContractID
		output.ContractRevision = i.ContractRevision
	case TypeClaim:
		output.Type = domain.OutputTypeClaim
		output.OutputTransactionID = &i.TransactionID
	case TypeDevClaim:
		output.Type = domain.OutputTypeDevClaim
		output.OutputTransactionID = &i.TransactionID
	}

	return output, true
}

func (bf *blockFacts) consumeTransactionFundInput(outputID types.SiafundOutputID) *types.TransactionID {
	key := crypto.Hash(outputID)
	transactionID, ok := bf.fundInputs[key]
	if ok {
		delete(bf.fundInputs, key)
		return &transactionID
	}
	return nil
}

func (bf *blockFacts) consumeTransactionFundOutput(outputID types.SiafundOutputID) *types.TransactionID {
	key := crypto.Hash(outputID)
	transactionID, ok := bf.fundOutputs[key]
	if ok {
		delete(bf.fundOutputs, key)
		return &transactionID
	}
	return nil
}

func (bf *blockFacts) consumeTransactionContract(contractID types.FileContractID) *types.TransactionID {
	key := crypto.Hash(contractID)
	transactionIDs, ok := bf.contracts[key]
	if !ok || len(transactionIDs) == 0 {
		return nil
	}
	transactionID := transactionIDs[0]
	bf.contracts[key] = transactionIDs[1:]
	return &transactionID
}

func (bf *blockFacts) transactionCoinInputs(txID types.TransactionID) []types.OutputID {
	if outputIDs, ok := bf.txInputs[txID]; ok {
		return outputIDs
	}
	return nil
}

func (bf *blockFacts) transactionCoinOutputs(txID types.TransactionID) []types.SiacoinOutput {
	if outputs, ok := bf.txOutputs[txID]; ok {
		return outputs
	}
	return nil
}

func (bf *blockFacts) outputByID(id types.OutputID) (types.SiacoinOutput, bool) {
	if so, ok := bf.outputs[id]; ok {
		return so, true
	}
	return types.SiacoinOutput{}, false
}
