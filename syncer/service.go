package syncer

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
	"gitlab.com/scpcorp/explorer/repository"
	"go.uber.org/zap"
)

var matViews = []string{
	repository.MatViewRootBalances,
	repository.MatViewDailyUserActivity,
	repository.MatViewDailyUnlockHashActivity,
	repository.MatViewDailyTransactions,
	repository.MatViewContractEconomy,
	repository.MatViewContractHostRevenueStats,
	repository.MatViewDailyInputs,
	repository.MatViewBlockPayouts,
	repository.MatViewDailyMovedCoins,
	repository.MatViewDailyMovedFunds,
	repository.MatViewTransactionFees,
	repository.MatViewDailyMedianTransactionFees,
	repository.MatViewContractFees,
	repository.MatViewDailyContracts,
	repository.MatViewTotalCoins,
	repository.MatViewUnspentCoins,
	repository.MatViewCoinBurns,
	repository.MatViewCoinBurnsStats,
	repository.MatViewUnspentFunds,
	repository.MatViewFundBurns,
	repository.MatViewFundBurnsStats,
	repository.MatViewClaimStats,
	repository.MatViewDailyContractedFilesize,
	repository.MatViewDailyContractFunding,
	repository.MatViewDailyScpPrice,
}

type service struct {
	l    *zap.Logger
	b    blockchainService
	r    repositoryService
	cerr chan error

	ctx context.Context

	viewsRefreshedAt time.Time
}

type blockchainService interface {
	Subscribe(subscriber modules.ConsensusSetSubscriber, start modules.ConsensusChangeID) error
	BlockByID(id types.BlockID) (types.Block, types.BlockHeight, bool)
	IsSiafundBOutput(id types.SiafundOutputID) (bool, error)
}

type repositoryService interface {
	Transaction(ctx context.Context, f func(repository.Repository) error) error

	repository.Repository
}

func New(l *zap.Logger, b blockchainService, r repositoryService) (*service, chan error) {
	s := &service{
		l:    l,
		b:    b,
		r:    r,
		cerr: make(chan error),
	}
	return s, s.cerr
}

func (s *service) Start(ctx context.Context) {
	s.ctx = ctx

	err := s.refreshMatViews()
	if err != nil {
		s.cerr <- err
	}

	startCCID, err := s.r.LatestConsensusChangeID(ctx)
	if errors.Is(err, domain.ErrNotFound) {
		startCCID = modules.ConsensusChangeBeginning
	} else if err != nil {
		s.cerr <- fmt.Errorf("r.LatestConsensusChangeID: %w", err)
		return
	}

	err = s.b.Subscribe(s, startCCID)
	if err != nil {
		s.cerr <- fmt.Errorf("b.Subscribe: %w", err)
		return
	}
}

// ProcessConsensusChange follows modules.ConsensusSetSubscriber interface.
func (s *service) ProcessConsensusChange(cc modules.ConsensusChange) {
	err := s.refreshMatViews()
	if err != nil {
		s.cerr <- err
	}

	p := newProcessor(s.l, s.ctx, s.b, s.r, cc)
	err = p.Process()
	if err != nil {
		s.cerr <- err
	}
}

func (s *service) refreshMatViews() error {
	if !s.viewsRefreshedAt.IsZero() && time.Since(s.viewsRefreshedAt) < 12*time.Hour {
		return nil
	}
	s.viewsRefreshedAt = time.Now()

	for _, view := range matViews {
		started := time.Now()
		err := s.r.RefreshMatView(s.ctx, view)
		if err != nil {
			return fmt.Errorf("r.RefreshMatView %s: %w", view, err)
		}
		s.l.Info("materialized view refreshed",
			zap.String("view", view),
			zap.Duration("took", time.Since(started)))
	}

	return nil
}
