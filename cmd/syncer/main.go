package main

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/jessevdk/go-flags"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/starius/api2"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/explorer/api"
	"gitlab.com/scpcorp/explorer/blockchain"
	"gitlab.com/scpcorp/explorer/logger"
	"gitlab.com/scpcorp/explorer/repository"
	"gitlab.com/scpcorp/explorer/syncer"
	"go.uber.org/zap"
)

type config struct {
	DbHost     string `long:"db-host" env:"DB_HOST"`
	DbPort     string `long:"db-port" env:"DB_PORT"`
	DbUser     string `long:"db-user" env:"DB_USER"`
	DbPassword string `long:"db-password" env:"DB_PASSWORD"`
	DbName     string `long:"db-name" env:"DB_NAME"`
	DbSslMode  string `long:"db-ssl-mode" env:"DB_SSL_MODE"`
	DbSchema   string `long:"db-schema" env:"DB_SCHEMA"`

	NodeDir string `long:"node-dir" env:"NODE_DIR"`
	ApiAddr string `long:"api-addr" env:"API_ADDR" default:":80"`
}

func main() {
	l := logger.New()

	// Parse flags.
	var cfg config
	args, err := flags.Parse(&cfg)
	if err != nil {
		l.Fatal("cannot parse config", zap.Error(err))
	}
	if len(args) != 0 {
		l.Fatal("arguments not expected", zap.Any("args", args))
	}

	// Main context.
	ctx := context.Background()

	// Initialize and start blockchain service.
	if cfg.NodeDir == "" {
		l.Fatal("empty NODE_DIR")
	}
	b, cerrb := blockchain.New(l.Named("Blockchain"), cfg.NodeDir)
	b.Start(ctx)
	if err := modules.PeekErr(cerrb); err != nil {
		l.Fatal("cannot initialize blockchain service",
			zap.Error(err))
	}

	// Make repository service.
	db, err := openPostgres(cfg)
	if err != nil {
		l.Fatal("cannot connect to database",
			zap.String("host", cfg.DbHost),
			zap.String("port", cfg.DbPort),
			zap.Error(err))
	}
	r := repository.New(l.Named("Repository"), db)

	// Initialize and start main service.
	s, cerrs := syncer.New(l, b, r)
	_ = s
	go func() {
		s.Start(ctx)
		if err := modules.PeekErr(cerrs); err != nil {
			l.Fatal("cannot initialize main service",
				zap.Error(err))
		}
	}()

	apiServer := api.NewServer(l.Named("API"), r)
	routes := api.Routes(apiServer)
	mux := http.NewServeMux()
	api2l := l.Named("api2")
	api2.BindRoutes(mux, routes, api2.ErrorLogger(func(format string, args ...interface{}) {
		api2l.Error(fmt.Sprintf(format, args...))
	}))

	httpServer := &http.Server{Addr: cfg.ApiAddr, Handler: mux}
	go func() {
		if err := httpServer.ListenAndServe(); err != nil {
			if err == http.ErrServerClosed {
				l.Info("server closed")
			} else {
				l.Fatal("cannot listen and serve", zap.Error(err))
			}
		}
	}()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, os.Kill, syscall.SIGTERM)

	select {
	case err := <-cerrb:
		l.Fatal("error from blockchain service",
			zap.Error(err))
	case err := <-cerrs:
		l.Fatal("error from main service",
			zap.Error(err))
	case <-sigChan:
		l.Fatal("caught stop signal")
	}
}

func openPostgres(cfg config) (*sql.DB, error) {
	psInfo := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=%s search_path=%s",
		cfg.DbHost,
		cfg.DbPort,
		cfg.DbUser,
		cfg.DbPassword,
		cfg.DbName,
		cfg.DbSslMode,
		cfg.DbSchema,
	)
	db, err := sql.Open("postgres", psInfo)
	if err != nil {
		return nil, fmt.Errorf("open: %w", err)
	}
	return db, nil
}
