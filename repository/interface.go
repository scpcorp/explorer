package repository

import (
	"context"

	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
)

type Repository interface {
	LatestConsensusChangeID(ctx context.Context) (modules.ConsensusChangeID, error)
	AddConsensusChange(ctx context.Context, id modules.ConsensusChangeID) error

	AddBlock(ctx context.Context, block domain.Block) error
	DeleteBlock(ctx context.Context, blockID types.BlockID) error

	AddTransaction(ctx context.Context, tx domain.Transaction) error
	AddTransactionThings(ctx context.Context, things []domain.TransactionThing) error
	DeleteTransactionThings(ctx context.Context, blockID types.BlockID) error
	DeleteTransactionsInBlock(ctx context.Context, blockID types.BlockID) error
	GetTransactionThings(ctx context.Context, txID types.TransactionID) ([]domain.TransactionThing, error)

	AddContract(ctx context.Context, contract domain.Contract) error
	DeleteContractsInBlock(ctx context.Context, blockID types.BlockID) error
	RevertContract(ctx context.Context, contractID types.FileContractID, contractRevision uint64, revertBlockID types.BlockID, revertHeight types.BlockHeight) error
	UnrevertContractsRevertedInBlock(ctx context.Context, revertBlockID types.BlockID) error
	SetHostPublicKey(ctx context.Context, contractID types.FileContractID, hostPublicKey types.SiaPublicKey) error

	AddStorageProof(ctx context.Context, proof domain.StorageProof) error

	AddOutput(ctx context.Context, output domain.Output) error
	UseOutput(ctx context.Context, outputID types.OutputID, blockID types.BlockID, height types.BlockHeight, transactionID *types.TransactionID) (domain.Output, error)
	UpdateOutput(ctx context.Context, output domain.Output) (domain.Output, error)
	DeleteOutputsInBlock(ctx context.Context, blockID types.BlockID) error
	IsFundbOutput(ctx context.Context, outputID types.OutputID) (bool, error)
	UnuseOutputsInBlock(ctx context.Context, inputBlockID types.BlockID) error
	OutputByID(ctx context.Context, outputID types.OutputID) (domain.Output, error)
	OutputsByIDs(ctx context.Context, outputIDs []types.OutputID) ([]domain.Output, error)
	UnlockHashOutputHeight(ctx context.Context, addr types.UnlockHash) (types.BlockHeight, error)

	AddArbitraryData(ctx context.Context, arb domain.ArbitraryData) error
	AddAnnouncement(ctx context.Context, ann domain.Announcement) error
	DeleteArbitraryDataInBlock(ctx context.Context, blockID types.BlockID) error
	DeleteAnnouncementsInBlock(ctx context.Context, blockID types.BlockID) error
	EarliestAnnouncement(ctx context.Context, pubKey types.SiaPublicKey) (domain.Announcement, error)

	IncrBalance(ctx context.Context, change domain.Balance) error
	DecrBalance(ctx context.Context, change domain.Balance) error

	RefreshMatView(ctx context.Context, mview string) error

	AddLinks(ctx context.Context, ls []domain.Link) error
	DeleteLinksInBlock(ctx context.Context, blockID types.BlockID) error
	DeleteRootsInBlock(ctx context.Context, blockID types.BlockID) error
	FindRoots(ctx context.Context, addresses []types.UnlockHash) (map[types.UnlockHash]domain.Root, error)
	UpdateRootAddress(ctx context.Context, oldRoot, newRoot types.UnlockHash) error
	AddRoots(ctx context.Context, rts []domain.Root) error
}
