package repository

import (
	"database/sql"
	"fmt"

	"github.com/jackc/pgtype"
	"gitlab.com/scpcorp/ScPrime/types"
)

type scanner interface {
	Scan(dest ...interface{}) error
	Err() error
}

func currencyToPgNumeric(c types.Currency) (pgtype.Numeric, error) {
	return toPgNumeric(c.String())
}

func toPgNumeric(x interface{}) (pgtype.Numeric, error) {
	var n pgtype.Numeric
	err := n.Set(x)
	if err != nil {
		return n, fmt.Errorf("cannot convert to pgtype.Numeric Value=%v: %w", x, err)
	}
	return n, nil
}

func toNullString(x interface{}) sql.NullString {
	if x == nil {
		return sql.NullString{}
	}
	switch x := x.(type) {
	case *types.BlockID:
		if x == nil {
			return sql.NullString{}
		}
		return sql.NullString{Valid: true, String: x.String()}
	case *types.TransactionID:
		if x == nil {
			return sql.NullString{}
		}
		return sql.NullString{Valid: true, String: x.String()}
	case *types.FileContractID:
		if x == nil {
			return sql.NullString{}
		}
		return sql.NullString{Valid: true, String: x.String()}
	default:
		panic(fmt.Sprintf("unknown type: %T", x))
	}
}

func toNullInt64(h *types.BlockHeight) sql.NullInt64 {
	n := sql.NullInt64{}
	if h != nil {
		n.Valid = true
		n.Int64 = int64(*h)
	}
	return n
}
