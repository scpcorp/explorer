package repository

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/scpcorp/explorer/domain"
	"gitlab.com/scpcorp/explorer/util"
)

const sqlLatestScpPrice = `
SELECT usd
FROM markets.coingecko_simple
ORDER BY last_updated_at DESC
LIMIT 1
`

func (r *repository) LatestScpPrice(ctx context.Context) (float64, error) {
	row := r.db.QueryRowContext(ctx, sqlLatestScpPrice)
	if err := row.Err(); err != nil {
		return 0, fmt.Errorf("query: %w", err)
	}

	var price float64
	err := row.Scan(&price)
	if err != nil {
		return 0, fmt.Errorf("scan: %w", err)
	}

	return price, nil
}

const sqlContractStats = `
SELECT
	filesize.time,
	filesize.count,
	filesize.filesize,
	funding.host_collateral,
	funding.renter_allowance,
	revenue.revenue,
	price.price
FROM daily_contracted_filesize filesize
LEFT JOIN daily_contract_funding funding ON funding.time = filesize.time
LEFT JOIN daily_scp_price price ON price.time = filesize.time
LEFT JOIN contract_host_revenue_stats revenue ON revenue.time = filesize.time
WHERE
	filesize.time < $1 AND
	filesize.filesize IS NOT NULL
ORDER BY filesize.time DESC
LIMIT $2
`

func (r *repository) ContractStats(ctx context.Context, before time.Time, limit int) ([]domain.ContractStat, error) {
	if before.IsZero() {
		before = util.BeginningOfUtcDay(time.Now())
	}

	rows, err := r.db.QueryContext(ctx, sqlContractStats, before, limit)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}
	defer rows.Close()

	var latestScpPrice float64
	var stats []domain.ContractStat
	first := true
	for rows.Next() {
		var raw rawContractStat
		if err := rows.Scan(
			&raw.Date,
			&raw.Count,
			&raw.Filesize,
			&raw.HostCollateral,
			&raw.RenterAllowance,
			&raw.Revenue,
			&raw.ScpPrice,
		); err != nil {
			return nil, fmt.Errorf("scan: %w", err)
		}

		price := raw.ScpPrice.Float64
		if price == 0 {
			if latestScpPrice == 0 {
				latestScpPrice, err = r.LatestScpPrice(ctx)
				if err != nil {
					return nil, fmt.Errorf("latest scp price: %w", err)
				}
			}
			if first {
				price = latestScpPrice
			}
		}

		stats = append(stats, domain.ContractStat{
			Date:               raw.Date,
			Contracts:          raw.Count,
			Bytes:              raw.Filesize,
			HostCollateral:     raw.HostCollateral.Currency,
			RenterAllowance:    raw.RenterAllowance.Currency,
			Revenue:            raw.Revenue.Currency,
			HostCollateralUsd:  price * util.CurrencyToScpFloat(raw.HostCollateral.Currency),
			RenterAllowanceUsd: price * util.CurrencyToScpFloat(raw.RenterAllowance.Currency),
			RevenueUsd:         price * util.CurrencyToScpFloat(raw.Revenue.Currency),
		})

		first = false
	}

	return stats, nil
}

type rawContractStat struct {
	Date            time.Time
	Count           int
	Filesize        int
	HostCollateral  SqlCurrency
	RenterAllowance SqlCurrency
	Revenue         SqlCurrency
	ScpPrice        sql.NullFloat64
}
