package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"math"
	"strings"

	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
	"gitlab.com/scpcorp/explorer/util"
)

const (
	defaultLinksLimit = 1000

	linkFields = `address1, address2, type, transaction_id, height`
	rootFields = `address, root, type, transaction_id, height`
)

var (
	linkN = len(strings.Split(linkFields, ","))
	rootN = len(strings.Split(rootFields, ","))
)

const fmtSqlAddLinks = `
INSERT INTO links (` + linkFields + `)
VALUES %s
ON CONFLICT DO NOTHING
`

func (r *repository) AddLinks(ctx context.Context, ls []domain.Link) error {
	args := make([]string, 0)
	bind := make([]interface{}, 0, len(ls)*linkN)
	for i, l := range ls {
		args = append(
			args,
			fmt.Sprintf(
				"($%d,$%d,$%d,$%d,$%d)",
				(i*linkN)+1, (i*linkN)+2, (i*linkN)+3, (i*linkN)+4, (i*linkN)+5,
			),
		)
		bind = append(
			bind,
			l.Address1.String(),
			l.Address2.String(),
			l.Type,
			l.TransactionID.String(),
			uint64(l.Height),
		)
	}
	query := fmt.Sprintf(fmtSqlAddLinks, strings.Join(args, ","))
	_, err := r.db.ExecContext(ctx, query, bind...)
	return err
}

const sqlGetLinks = `
SELECT ` + linkFields + `
FROM links
ORDER BY height, transaction_id, address1, address2
OFFSET $1 LIMIT $2
`

func (r *repository) GetLinks(ctx context.Context, offset int) ([]domain.Link, error) {
	rows, err := r.db.QueryContext(ctx, sqlGetLinks, offset, defaultLinksLimit)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}
	defer rows.Close()

	links := make([]domain.Link, 0, defaultLinksLimit)
	for rows.Next() {
		link, err := scanLink(rows)
		if err != nil {
			return nil, fmt.Errorf("scan: %w", err)
		}
		links = append(links, link)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("rows: %w", err)
	}

	return links, nil
}

const fmtSqlFindRoots = `
SELECT ` + rootFields + ` FROM roots
WHERE address IN (%s)
`

func (r *repository) FindRoots(ctx context.Context, addresses []types.UnlockHash) (map[types.UnlockHash]domain.Root, error) {
	args := make([]string, 0, len(addresses))
	bind := make([]interface{}, 0, len(addresses))
	for i, addr := range addresses {
		args = append(args, fmt.Sprintf("$%d", i+1))
		bind = append(bind, addr.String())
	}

	query := fmt.Sprintf(fmtSqlFindRoots, strings.Join(args, ","))
	rows, err := r.db.QueryContext(ctx, query, bind...)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}
	defer rows.Close()

	rtm := make(map[types.UnlockHash]domain.Root)
	for rows.Next() {
		rt, err := scanRoot(rows)
		if err != nil {
			return nil, fmt.Errorf("scan: %w", err)
		}
		rtm[rt.Address] = rt
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("rows: %w", err)
	}

	return rtm, err
}

const fmtSqlAddRoots = `
INSERT INTO roots (` + rootFields + `)
VALUES %s
ON CONFLICT DO NOTHING
`

func (r *repository) AddRoots(ctx context.Context, rts []domain.Root) error {
	args := make([]string, 0)
	bind := make([]interface{}, 0, len(rts)*rootN)
	for i, rt := range rts {
		args = append(
			args,
			fmt.Sprintf(
				"($%d,$%d,$%d,$%d,$%d)",
				(i*rootN)+1, (i*rootN)+2, (i*rootN)+3, (i*rootN)+4, (i*rootN)+5,
			),
		)
		bind = append(
			bind,
			rt.Address.String(),
			rt.Root.String(),
			rt.Type,
			rt.TransactionID.String(),
			uint64(rt.Height),
		)
	}
	query := fmt.Sprintf(fmtSqlAddRoots, strings.Join(args, ","))
	_, err := r.db.ExecContext(ctx, query, bind...)
	return err
}

const sqlUpdateRootAddress = `
UPDATE roots
SET root = $2
WHERE root = $1
`

func (r *repository) UpdateRootAddress(ctx context.Context, oldRoot, newRoot types.UnlockHash) error {
	_, err := r.db.ExecContext(ctx, sqlUpdateRootAddress,
		oldRoot.String(),
		newRoot.String())
	if err != nil {
		return fmt.Errorf("exec: %w", err)
	}
	return nil
}

const fmtSqlFindAddressOutputHeights = `
SELECT unlock_hash, MIN(output_height)
FROM outputs
WHERE unlock_hash IN (%s)
GROUP BY unlock_hash
`

func (r *repository) FindEarliestAddress(ctx context.Context, addresses []types.UnlockHash) (types.UnlockHash, error) {
	if len(addresses) == 0 {
		panic("no addresses provided")
	}

	args := make([]string, 0, len(addresses))
	bind := make([]interface{}, 0, len(addresses))
	for i, addr := range addresses {
		args = append(args, fmt.Sprintf("$%d", i+1))
		bind = append(bind, addr.String())
	}

	query := fmt.Sprintf(fmtSqlFindAddressOutputHeights, strings.Join(args, ","))
	rows, err := r.db.QueryContext(ctx, query, bind...)
	if err != nil {
		return types.UnlockHash{}, fmt.Errorf("query: %w", err)
	}
	defer rows.Close()

	var minAddrStr string
	var minHeight uint64 = math.MaxUint64
	var addrStr string
	var height uint64
	for rows.Next() {
		err := rows.Scan(&addrStr, &height)
		if err != nil {
			return types.UnlockHash{}, fmt.Errorf("scan: %w", err)
		}
		if height < minHeight {
			minHeight = height
			minAddrStr = addrStr
		}
	}

	if err := rows.Err(); err != nil {
		return types.UnlockHash{}, fmt.Errorf("rows: %w", err)
	}

	if minAddrStr == "" {
		panic("couldn't find address")
	}
	addr, err := util.ScanUnlockHash(minAddrStr)
	if err != nil {
		return types.UnlockHash{}, fmt.Errorf("hash: %w", err)
	}

	return addr, nil
}

func scanLink(row scanner) (domain.Link, error) {
	var addr1Str, addr2Str, typ, txIDStr string
	var height uint64
	if err := row.Scan(
		&addr1Str,
		&addr2Str,
		&typ,
		&txIDStr,
		&height,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return domain.Link{}, domain.ErrNotFound
		}
		return domain.Link{}, fmt.Errorf("scan: %w", err)
	}

	if err := row.Err(); err != nil {
		return domain.Link{}, fmt.Errorf("rowErr: %w", err)
	}

	l := domain.Link{
		Type:   typ,
		Height: types.BlockHeight(height),
	}

	var err error

	l.Address1, err = util.ScanUnlockHash(addr1Str)
	if err != nil {
		return domain.Link{}, fmt.Errorf("scan addr1: %w", err)
	}

	l.Address2, err = util.ScanUnlockHash(addr2Str)
	if err != nil {
		return domain.Link{}, fmt.Errorf("scan addr2: %w", err)
	}

	l.TransactionID, err = util.ScanTransactionID(txIDStr)
	if err != nil {
		return domain.Link{}, fmt.Errorf("scan txid: %w", err)
	}

	return l, nil
}

func scanRoot(row scanner) (domain.Root, error) {
	var addrStr, rootStr, typ, txIDStr string
	var height uint64
	if err := row.Scan(
		&addrStr,
		&rootStr,
		&typ,
		&txIDStr,
		&height,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return domain.Root{}, domain.ErrNotFound
		}
		return domain.Root{}, fmt.Errorf("scan: %w", err)
	}

	if err := row.Err(); err != nil {
		return domain.Root{}, fmt.Errorf("rowErr: %w", err)
	}

	r := domain.Root{
		Type:   typ,
		Height: types.BlockHeight(height),
	}

	var err error

	r.Address, err = util.ScanUnlockHash(addrStr)
	if err != nil {
		return domain.Root{}, fmt.Errorf("scan addr: %w", err)
	}

	r.Root, err = util.ScanUnlockHash(rootStr)
	if err != nil {
		return domain.Root{}, fmt.Errorf("scan root: %w", err)
	}

	r.TransactionID, err = util.ScanTransactionID(txIDStr)
	if err != nil {
		return domain.Root{}, fmt.Errorf("scan txid: %w", err)
	}

	return r, nil
}
