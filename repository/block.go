package repository

import (
	"context"

	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
)

const blockFields = `id, cc_id, height, parent_id, timestamp`

const sqlAddBlock = `
INSERT INTO blocks (` + blockFields + `)
VALUES ($1, $2, $3, $4, $5)
`

func (r *repository) AddBlock(ctx context.Context, block domain.Block) error {
	_, err := r.db.ExecContext(ctx, sqlAddBlock,
		block.ID.String(),
		block.CCID.String(),
		uint64(block.Height),
		toNullString(block.ParentID),
		block.Timestamp,
	)
	return err
}

const sqlDeleteBlock = `
DELETE FROM blocks WHERE id = $1
`

func (r *repository) DeleteBlock(ctx context.Context, blockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlDeleteBlock, blockID.String())
	return err
}
