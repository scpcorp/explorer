package repository

import (
	"context"
	"fmt"

	"gitlab.com/scpcorp/explorer/domain"
)

const sqlBalanceIncrCoins = `
INSERT INTO balances (address, coin_balance, funda_balance, fundb_balance)
VALUES ($1, $2, 0, 0)
ON CONFLICT (address) DO UPDATE
SET coin_balance = balances.coin_balance + EXCLUDED.coin_balance
`

const sqlBalanceIncrFunda = `
INSERT INTO balances (address, coin_balance, funda_balance, fundb_balance)
VALUES ($1, 0, $2, 0)
ON CONFLICT (address) DO UPDATE
SET funda_balance = balances.funda_balance + EXCLUDED.funda_balance
`

const sqlBalanceIncrFundb = `
INSERT INTO balances (address, coin_balance, funda_balance, fundb_balance)
VALUES ($1, 0, 0, $2)
ON CONFLICT (address) DO UPDATE
SET fundb_balance = balances.fundb_balance + EXCLUDED.fundb_balance
`

const sqlBalanceDecrCoins = `
INSERT INTO balances (address, coin_balance, funda_balance, fundb_balance)
VALUES ($1, $2, 0, 0)
ON CONFLICT (address) DO UPDATE
SET coin_balance = balances.coin_balance - EXCLUDED.coin_balance
`

const sqlBalanceDecrFunda = `
INSERT INTO balances (address, coin_balance, funda_balance, fundb_balance)
VALUES ($1, 0, $2, 0)
ON CONFLICT (address) DO UPDATE
SET funda_balance = balances.funda_balance - EXCLUDED.funda_balance
`

const sqlBalanceDecrFundb = `
INSERT INTO balances (address, coin_balance, funda_balance, fundb_balance)
VALUES ($1, 0, 0, $2)
ON CONFLICT (address) DO UPDATE
SET fundb_balance = balances.fundb_balance - EXCLUDED.fundb_balance
`

func ternary(cond bool, thenResult, elseResult string) string {
	if cond {
		return thenResult
	} else {
		return elseResult
	}
}

func (r *repository) changeBalance(ctx context.Context, incr bool, change domain.Balance) error {
	changes := 0
	if !change.CoinBalance.IsZero() {
		changes++
	}
	if !change.FundaBalance.IsZero() {
		changes++
	}
	if !change.FundbBalance.IsZero() {
		changes++
	}
	if changes == 0 {
		return nil
	}
	if changes > 1 {
		return fmt.Errorf("more than one change")
	}

	switch {
	case !change.CoinBalance.IsZero():
		coin, err := currencyToPgNumeric(change.CoinBalance)
		if err != nil {
			return fmt.Errorf("cannot convert CoinBalance=%s to pgtype.Numeric: %w", change.CoinBalance, err)
		}

		query := ternary(incr, sqlBalanceIncrCoins, sqlBalanceDecrCoins)
		_, err = r.db.ExecContext(ctx, query, change.Address.String(), coin)
		if err != nil {
			return fmt.Errorf("cannot update coins Address=%s Coins=%s: %w", change.Address, change.CoinBalance, err)
		}
	case !change.FundaBalance.IsZero():
		funda64, err := change.FundaBalance.Uint64()
		if err != nil {
			return fmt.Errorf("cannot convert FundaBalance=%s to uint64: %w", change.FundaBalance, err)
		}
		query := ternary(incr, sqlBalanceIncrFunda, sqlBalanceDecrFunda)
		_, err = r.db.ExecContext(ctx, query, change.Address.String(), funda64)
		if err != nil {
			return fmt.Errorf("cannot update coins Address=%s Funda=%s: %w", change.Address, change.FundaBalance, err)
		}
	case !change.FundbBalance.IsZero():
		fundb64, err := change.FundbBalance.Uint64()
		if err != nil {
			return fmt.Errorf("cannot convert FundbBalance=%s to uint64: %w", change.FundbBalance, err)
		}
		query := ternary(incr, sqlBalanceIncrFundb, sqlBalanceDecrFundb)
		_, err = r.db.ExecContext(ctx, query, change.Address.String(), fundb64)
		if err != nil {
			return fmt.Errorf("cannot update coins Address=%s Fundb=%s: %w", change.Address, change.FundbBalance, err)
		}
	}

	return nil
}

func (r *repository) IncrBalance(ctx context.Context, change domain.Balance) error {
	return r.changeBalance(ctx, true, change)
}

func (r *repository) DecrBalance(ctx context.Context, change domain.Balance) error {
	return r.changeBalance(ctx, false, change)
}
