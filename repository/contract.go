package repository

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jackc/pgtype"
	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
	"gitlab.com/scpcorp/explorer/util"
)

const contractFields = `
	id, revision, filesize, window_start, window_end,
	payout, unlock_hash, host_public_key, block_id, block_height, transaction_id,
	revert_block_id, revert_height,
	valid_renter_payout, valid_renter_address, valid_renter_output_id,
	valid_host_payout, valid_host_address, valid_host_output_id,
	missed_renter_payout, missed_renter_address, missed_renter_output_id,
	missed_host_payout, missed_host_address, missed_host_output_id,
	missed_void_payout, missed_void_address, missed_void_output_id,
	host_collateral, renter_allowance
`

const sqlAddContract = `
INSERT INTO contracts (` + contractFields + `)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16,
	$17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $30)
`

func (r *repository) AddContract(ctx context.Context, contract domain.Contract) error {
	payoutNumeric, err := currencyToPgNumeric(contract.Payout)
	if err != nil {
		return fmt.Errorf("cannot convert Payout=%s to pgtype.Numeric: %w", contract.Payout, err)
	}

	// sql: "uint64 values with high bit set are not supported"
	revisionNum, err := toPgNumeric(contract.Revision)
	if err != nil {
		return fmt.Errorf("cannot convert Revision=%d to pgtype.Numeric: %w", contract.Revision, err)
	}

	filesizeNum, err := toPgNumeric(contract.Filesize)
	if err != nil {
		return fmt.Errorf("cannot convert Filesize=%d to pgtype.Numeric: %w", contract.Filesize, err)
	}

	validRenterPayout, err := currencyToPgNumeric(contract.ValidRenterPayout)
	if err != nil {
		return fmt.Errorf("cannot convert ValidRenterPayout=%s to pgtype.Numeric: %w", contract.ValidRenterPayout, err)
	}
	validHostPayout, err := currencyToPgNumeric(contract.ValidHostPayout)
	if err != nil {
		return fmt.Errorf("cannot convert ValidHostPayout=%s to pgtype.Numeric: %w", contract.ValidHostPayout, err)
	}
	missedRenterPayout, err := currencyToPgNumeric(contract.MissedRenterPayout)
	if err != nil {
		return fmt.Errorf("cannot convert MissedRenterPayout=%s to pgtype.Numeric: %w", contract.MissedRenterPayout, err)
	}
	missedHostPayout, err := currencyToPgNumeric(contract.MissedHostPayout)
	if err != nil {
		return fmt.Errorf("cannot convert MissedHostPayout=%s to pgtype.Numeric: %w", contract.MissedHostPayout, err)
	}
	missedVoidPayout, err := currencyToPgNumeric(contract.MissedVoidPayout)
	if err != nil {
		return fmt.Errorf("cannot convert MissedVoidPayout=%s to pgtype.Numeric: %w", contract.MissedVoidPayout, err)
	}
	hostPublicKey := sql.NullString{}
	if contract.HostPublicKey != nil {
		hostPublicKey.Valid = true
		hostPublicKey.String = contract.HostPublicKey.String()
	}

	var hostCollateral, renterAllowance *pgtype.Numeric
	if contract.HostCollateral.Cmp(types.ZeroCurrency) > 0 &&
		contract.RenterAllowance.Cmp(types.ZeroCurrency) > 0 {
		hc, err := currencyToPgNumeric(contract.HostCollateral)
		if err != nil {
			return fmt.Errorf("cannot convert HostCollateral=%s to pgtype.Numeric: %w", contract.HostCollateral, err)
		}
		ra, err := currencyToPgNumeric(contract.RenterAllowance)
		if err != nil {
			return fmt.Errorf("cannot convert RenterAllowance=%s to pgtype.Numeric: %w", contract.RenterAllowance, err)
		}
		hostCollateral, renterAllowance = &hc, &ra
	}

	_, err = r.db.ExecContext(ctx, sqlAddContract,
		contract.ID.String(),
		revisionNum,
		filesizeNum,
		uint64(contract.WindowStart),
		uint64(contract.WindowEnd),
		payoutNumeric,
		contract.UnlockHash.String(),
		hostPublicKey,
		contract.BlockID.String(),
		uint64(contract.Height),
		toNullString(contract.TransactionID),
		toNullString(contract.RevertBlockID),
		toNullInt64(contract.RevertHeight),
		validRenterPayout,
		contract.ValidRenterAddress.String(),
		contract.ValidRenterOutputID.String(),
		validHostPayout,
		contract.ValidHostAddress.String(),
		contract.ValidHostOutputID.String(),
		missedRenterPayout,
		contract.MissedRenterAddress.String(),
		contract.MissedRenterOutputID.String(),
		missedHostPayout,
		contract.MissedHostAddress.String(),
		contract.MissedHostOutputID.String(),
		missedVoidPayout,
		contract.MissedVoidAddress.String(),
		contract.MissedVoidOutputID.String(),
		hostCollateral,
		renterAllowance,
	)
	if err != nil {
		return fmt.Errorf("db: %w", err)
	}

	return nil
}

const sqlDeleteContractsInBlock = `
DELETE FROM contracts WHERE block_id = $1
`

func (r *repository) DeleteContractsInBlock(ctx context.Context, blockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlDeleteContractsInBlock,
		blockID.String())
	return err
}

const sqlRevertContract = `
UPDATE contracts
SET
	revert_block_id = $3,
	revert_height = $4
WHERE id = $1 AND revision = $2 AND revert_block_id IS NULL
`

func (r *repository) RevertContract(ctx context.Context, contractID types.FileContractID, contractRevision uint64, revertBlockID types.BlockID, revertHeight types.BlockHeight) error {
	revisionNum, err := toPgNumeric(contractRevision)
	if err != nil {
		return fmt.Errorf("toPgNumeric contractRevision=%d: %w", contractRevision, err)
	}
	res, err := r.db.ExecContext(ctx, sqlRevertContract,
		contractID.String(),
		revisionNum,
		revertBlockID.String(),
		uint64(revertHeight))
	if err != nil {
		return fmt.Errorf("db: %w", err)
	}
	affected, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("rowsAffected: %w", err)
	}
	if affected == 0 {
		return domain.ErrNotFound
	}
	return nil
}

const sqlUnrevertContractsInBlock = `
UPDATE contracts
SET
	revert_block_id = NULL,
	revert_height = NULL
WHERE revert_block_id = $1
`

func (r *repository) UnrevertContractsRevertedInBlock(ctx context.Context, revertBlockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlUnrevertContractsInBlock,
		revertBlockID.String())
	return err
}

const sqlSetHostPublicKey = `
UPDATE contracts
SET host_public_key = $2
WHERE id = $1
`

func (r *repository) SetHostPublicKey(ctx context.Context, contractID types.FileContractID, hostPublicKey types.SiaPublicKey) error {
	_, err := r.db.ExecContext(ctx, sqlSetHostPublicKey,
		contractID.String(),
		util.RemovePkSpecifier(hostPublicKey.String()))
	if err != nil {
		return fmt.Errorf("exec: %w", err)
	}
	return nil
}
