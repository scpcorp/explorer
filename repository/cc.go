package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/explorer/domain"
)

const sqlLatestConsensusChange = `
SELECT cc_id FROM consensus_changes ORDER BY id DESC LIMIT 1
`

var emptyConsensusChangeID = modules.ConsensusChangeID{}

func (r *repository) LatestConsensusChangeID(ctx context.Context) (modules.ConsensusChangeID, error) {
	row := r.db.QueryRowContext(ctx, sqlLatestConsensusChange)

	var s sql.NullString
	err := row.Scan(&s)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return emptyConsensusChangeID, domain.ErrNotFound
		}
		return emptyConsensusChangeID, fmt.Errorf("scan: %w", err)
	}
	if !s.Valid {
		return emptyConsensusChangeID, domain.ErrNotFound
	}

	if err := row.Err(); err != nil {
		return emptyConsensusChangeID, fmt.Errorf("rowErr: %w", err)
	}

	var h crypto.Hash
	err = h.LoadString(s.String)
	if err != nil {
		return emptyConsensusChangeID, fmt.Errorf("h.LoadString cc_id=%s: %w", s.String, err)
	}
	return modules.ConsensusChangeID(h), nil
}

const sqlAddConsensusChange = `
INSERT INTO consensus_changes (cc_id)
VALUES ($1)
`

func (r *repository) AddConsensusChange(ctx context.Context, id modules.ConsensusChangeID) error {
	_, err := r.db.ExecContext(ctx, sqlAddConsensusChange,
		id.String())
	if err != nil {
		return fmt.Errorf("sqlAddConsensusChange: %w", err)
	}

	return nil
}
