package repository

import (
	"context"

	"gitlab.com/scpcorp/explorer/domain"
)

const sqlAddStorageProof = `
INSERT INTO storage_proofs (contract_id, transaction_id, block_id, height)
VALUES ($1, $2, $3, $4)
`

func (r *repository) AddStorageProof(ctx context.Context, proof domain.StorageProof) error {
	_, err := r.db.ExecContext(ctx, sqlAddStorageProof,
		proof.ContractID.String(),
		proof.TransactionID.String(),
		proof.BlockID.String(),
		uint64(proof.Height))
	return err
}
