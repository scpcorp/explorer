package repository

import (
	"context"
	"database/sql"
	"encoding/hex"
	"errors"
	"fmt"

	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
	"gitlab.com/scpcorp/explorer/util"
)

const sqlAddArbitraryData = `
INSERT INTO arbitrary_data (block_id, transaction_id, data_index, data)
VALUES ($1, $2, $3, $4)
`

func (r *repository) AddArbitraryData(ctx context.Context, arb domain.ArbitraryData) error {
	r.requireTX()
	for i, value := range arb.Data {
		_, err := r.db.ExecContext(ctx, sqlAddArbitraryData,
			arb.BlockID.String(),
			arb.TransactionID.String(),
			i,
			value)
		if err != nil {
			return fmt.Errorf("sqlAddArbitraryData: %w", err)
		}
	}
	return nil
}

const announcementFields = `public_key, transaction_id, data_index, block_id, height, address`

const sqlAddAnnouncement = `
INSERT INTO announcements (` + announcementFields + `)
VALUES ($1, $2, $3, $4, $5, $6)
`

func (r *repository) AddAnnouncement(ctx context.Context, ann domain.Announcement) error {
	buf := make([]byte, 64)
	hex.Encode(buf, ann.PublicKey.Key)
	pk := string(buf)
	_, err := r.db.ExecContext(ctx, sqlAddAnnouncement,
		pk,
		ann.TransactionID.String(),
		ann.DataIndex,
		ann.BlockID.String(),
		uint64(ann.Height),
		ann.Address)
	if err != nil {
		return fmt.Errorf("sqlAddAnnouncement: %w", err)
	}
	return nil
}

const sqlDeleteArbitraryDataInBlock = `
DELETE FROM arbitrary_data
WHERE block_id = $1
`

func (r *repository) DeleteArbitraryDataInBlock(ctx context.Context, blockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlDeleteArbitraryDataInBlock,
		blockID.String())
	return err
}

const sqlDeleteAnnouncementsInBlock = `
DELETE FROM announcements
WHERE block_id = $1
`

func (r *repository) DeleteAnnouncementsInBlock(ctx context.Context, blockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlDeleteAnnouncementsInBlock,
		blockID.String())
	return err
}

const sqlEarliestAnnouncement = `
SELECT ` + announcementFields + `
FROM announcements
WHERE public_key = $1
ORDER BY height
LIMIT 1
`

func (r *repository) EarliestAnnouncement(ctx context.Context, pubKey types.SiaPublicKey) (domain.Announcement, error) {
	pk := util.RemovePkSpecifier(pubKey.String())
	row := r.db.QueryRowContext(ctx, sqlEarliestAnnouncement, pk)
	return scanAnnouncement(row)
}

func scanAnnouncement(row scanner) (domain.Announcement, error) {
	var publicKeyStr, transactionIDStr, blockIDStr, addressStr string
	var dataIndex, height int
	err := row.Scan(&publicKeyStr, &transactionIDStr, &dataIndex, &blockIDStr, &height, &addressStr)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return domain.Announcement{}, domain.ErrNotFound
		}
		return domain.Announcement{}, fmt.Errorf("scan: %w", err)
	}
	a := domain.Announcement{}
	a.PublicKey, err = util.ScanPublicKey(publicKeyStr)
	if err != nil {
		return domain.Announcement{}, fmt.Errorf("scan pk '%s': %w", publicKeyStr, err)
	}
	a.TransactionID, err = util.ScanTransactionID(transactionIDStr)
	if err != nil {
		return domain.Announcement{}, fmt.Errorf("scan tx '%s': %w", transactionIDStr, err)
	}
	a.DataIndex = dataIndex
	a.BlockID, err = util.ScanBlockID(blockIDStr)
	if err != nil {
		return domain.Announcement{}, fmt.Errorf("scan block '%s': %w", blockIDStr, err)
	}
	a.Height = types.BlockHeight(height)
	a.Address = modules.NetAddress(addressStr)
	return a, nil
}
