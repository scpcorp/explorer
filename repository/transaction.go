package repository

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
	"gitlab.com/scpcorp/explorer/util"
)

const transactionFields = `id, block_id, tx_type`

const sqlAddTransaction = `
INSERT INTO transactions (` + transactionFields + `)
VALUES ($1, $2, $3)
`

func (r *repository) AddTransaction(ctx context.Context, tx domain.Transaction) error {
	_, err := r.db.ExecContext(ctx, sqlAddTransaction,
		tx.ID.String(),
		tx.BlockID.String(),
		tx.TxType)
	return err
}

const sqlDeleteLinksInBlock = `
DELETE FROM links
WHERE transaction_id IN (SELECT id FROM transactions WHERE block_id = $1)
`

func (r *repository) DeleteLinksInBlock(ctx context.Context, blockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlDeleteLinksInBlock,
		blockID.String())
	return err
}

const sqlDeleteRootsInBlock = `
DELETE FROM roots
WHERE transaction_id IN (SELECT id FROM transactions WHERE block_id = $1)
`

func (r *repository) DeleteRootsInBlock(ctx context.Context, blockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlDeleteRootsInBlock,
		blockID.String())
	return err
}

const sqlDeleteTransactionsInBlock = `
DELETE FROM transactions
WHERE block_id = $1
`

func (r *repository) DeleteTransactionsInBlock(ctx context.Context, blockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlDeleteTransactionsInBlock,
		blockID.String())
	return err
}

const sqlDeleteTransactionsThings = `
DELETE FROM transaction_things
WHERE transaction_id IN (SELECT id FROM transactions WHERE block_id = $1)
`

func (r *repository) DeleteTransactionThings(ctx context.Context, blockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlDeleteTransactionsThings,
		blockID.String())
	return err
}

const fmtSqlAddTransactionThings = `
INSERT INTO transaction_things (transaction_id, thing, index, thing_id)
VALUES %s
`

func (r *repository) AddTransactionThings(ctx context.Context, things []domain.TransactionThing) error {
	const n = 4
	args := make([]string, 0)
	bind := make([]interface{}, 0, len(things)*n)
	for i, th := range things {
		args = append(
			args,
			fmt.Sprintf(
				"($%d, $%d, $%d, $%d)",
				(i*n)+1, (i*n)+2, (i*n)+3, (i*n)+4,
			),
		)
		bind = append(
			bind,
			th.TransactionID.String(),
			th.Thing,
			th.Index,
			th.ThingID.String(),
		)
	}
	query := fmt.Sprintf(fmtSqlAddTransactionThings, strings.Join(args, ","))
	_, err := r.db.ExecContext(ctx, query, bind...)
	return err
}

const sqlSelectTransactionThings = `
SELECT transaction_id, thing, index, thing_id
FROM transaction_things
WHERE transaction_id = $1
`

func (r *repository) GetTransactionThings(ctx context.Context, txID types.TransactionID) ([]domain.TransactionThing, error) {
	rows, err := r.db.QueryContext(ctx, sqlSelectTransactionThings, txID.String())
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}
	defer rows.Close()

	var transactionIDStr, thing, thingIDStr string
	var index int
	var tts []domain.TransactionThing
	for rows.Next() {
		err = rows.Scan(&transactionIDStr, &thing, &index, &thingIDStr)
		if err != nil {
			return nil, fmt.Errorf("scan: %w", err)
		}

		var tt domain.TransactionThing
		tt.TransactionID, err = util.ScanTransactionID(transactionIDStr)
		if err != nil {
			return nil, fmt.Errorf("scan tx '%s': %w", transactionIDStr, err)
		}
		tt.Thing = thing
		tt.Index = index
		tt.ThingID, err = util.ScanHash(thingIDStr)
		if err != nil {
			return nil, fmt.Errorf("scan thing id '%s': %w", thingIDStr, err)
		}

		tts = append(tts, tt)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("rows: %w", err)
	}

	return tts, nil
}

//
//const sqlUpdateTransactionType = `
//UPDATE transactions
//SET type = $2
//WHERE id = $1
//`
//
//func (r *repository) UpdateTransactionType(ctx context.Context, txID types.TransactionID, typ string) error {
//	res, err := r.db.ExecContext(ctx, sqlUpdateTransactionType,
//		txID.String(),
//		typ,
//	)
//	if err != nil {
//		return fmt.Errorf("exec: %w", err)
//	}
//	affectedRows, err := res.RowsAffected()
//	if err != nil {
//		return fmt.Errorf("affected: %w", err)
//	}
//	if affectedRows != 1 {
//		return fmt.Errorf("bad affected rows: %d", affectedRows)
//	}
//	return nil
//}

//const sqlUpdateTransaction = `
//UPDATE transactions
//SET
//	block_id = $2,
//	type = $3,
//	related_id = $4
//WHERE id = $1
//`
//
//func (r *repository) UpdateTransaction(ctx context.Context, tx domain.Transaction) error {
//	var relatedID sql.NullString
//	if tx.RelatedID != nil {
//		relatedID.String = tx.RelatedID.String()
//		relatedID.Valid = true
//	}
//	res, err := r.db.ExecContext(ctx, sqlUpdateTransaction,
//		tx.ID.String(),
//		tx.BlockID.String(),
//		tx.Type,
//		relatedID,
//	)
//	if err != nil {
//		return fmt.Errorf("exec: %w", err)
//	}
//	affectedRows, err := res.RowsAffected()
//	if err != nil {
//		return fmt.Errorf("affected: %w", err)
//	}
//	if affectedRows != 1 {
//		return fmt.Errorf("bad affected rows with tx '%s': %d", tx.ID.String(), affectedRows)
//	}
//	return nil
//}
