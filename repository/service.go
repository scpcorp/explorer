package repository

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"errors"
	"fmt"
	"math/big"

	"gitlab.com/scpcorp/ScPrime/types"
	"go.uber.org/zap"
)

type repository struct {
	l  *zap.Logger
	db db
}

type db interface {
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error)
}

func New(l *zap.Logger, db *sql.DB) *repository {
	return &repository{
		l:  l,
		db: db,
	}
}

// txdb is a wrapper around sql.Tx with addition of (not working) BeginTx
// which makes it possible to use it as db.
type txdb struct {
	*sql.Tx
}

var _ db = &txdb{nil}

func (*txdb) BeginTx(context.Context, *sql.TxOptions) (*sql.Tx, error) {
	return nil, fmt.Errorf("already in a transaction")
}

// Transaction runs given function inside database transaction.
func (r *repository) Transaction(ctx context.Context, f func(r Repository) error) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("db.BeginTx: %w", err)
	}
	defer func() {
		err := tx.Rollback()
		if err != nil && !errors.Is(err, sql.ErrTxDone) {
			r.l.Error("cannot rollback", zap.Error(err))
		}
	}()

	db2 := txdb{tx}
	r2 := &repository{db: &db2}

	err = f(r2)
	if err != nil {
		return fmt.Errorf("error in transaction call: %w", err)
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("tx.Commit: %w", err)
	}

	return nil
}

func (r *repository) requireTX() {
	_, ok := r.db.(*txdb)
	if !ok {
		panic("must be run inside transaction")
	}
}

type SqlCurrency struct {
	types.Currency
}

// Scan implements the database/sql Scanner interface.
func (sc *SqlCurrency) Scan(src interface{}) error {
	if src == nil {
		sc.Currency = types.NewCurrency64(0)
		return nil
	}

	switch src := src.(type) {
	case string:
		if src == "" {
			// Backwards compatibility.
			sc.Currency = types.NewCurrency64(0)
			return nil
		}
		return sc.setFromString(src)
	case []byte:
		return sc.setFromString(string(src))
	}

	return fmt.Errorf("cannot scan %T", src)
}

func (sc *SqlCurrency) setFromString(s string) error {
	i := new(big.Int)
	_, ok := i.SetString(s, 10)
	if !ok {
		return fmt.Errorf("cannot scan (bigint): %v", s)
	}
	sc.Currency = types.NewCurrency(i)
	return nil
}

// Value implements the database/sql/driver Valuer interface.
func (sc *SqlCurrency) Value() (driver.Value, error) {
	return sc.String(), nil
}
