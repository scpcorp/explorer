package repository

import (
	"context"
	"fmt"
)

const (
	MatViewRootBalances               = "root_balances"
	MatViewDailyUserActivity          = "daily_user_activity"
	MatViewDailyUnlockHashActivity    = "daily_unlock_hash_activity"
	MatViewDailyTransactions          = "daily_transactions"
	MatViewDailyInputs                = "daily_inputs"
	MatViewContractEconomy            = "contract_economy"
	MatViewContractHostRevenueStats   = "contract_host_revenue_stats"
	MatViewBlockPayouts               = "block_payouts"
	MatViewDailyMovedCoins            = "daily_moved_coins"
	MatViewDailyMovedFunds            = "daily_moved_funds"
	MatViewTransactionFees            = "transaction_fees"
	MatViewDailyMedianTransactionFees = "daily_median_transaction_fees"
	MatViewContractFees               = "contract_fees"
	MatViewDailyContracts             = "daily_contracts2"
	MatViewTotalCoins                 = "total_coins"
	MatViewUnspentCoins               = "unspent_coins"
	MatViewCoinBurns                  = "coin_burns"
	MatViewCoinBurnsStats             = "coin_burns_stats"
	MatViewUnspentFunds               = "unspent_funds"
	MatViewFundBurns                  = "fund_burns"
	MatViewFundBurnsStats             = "fund_burns_stats"
	MatViewClaimStats                 = "claim_stats"
	MatViewDailyContractedFilesize    = "daily_contracted_filesize"
	MatViewDailyContractFunding       = "daily_contract_funding"
	MatViewDailyScpPrice              = "daily_scp_price"
)

const fmtSqlRefreshMatView = `
REFRESH MATERIALIZED VIEW CONCURRENTLY %s
`

func (r *repository) RefreshMatView(ctx context.Context, mview string) error {
	_, err := r.db.ExecContext(ctx, fmt.Sprintf(fmtSqlRefreshMatView, mview))
	if err != nil {
		return fmt.Errorf("cannot refresh mview %s: %w", mview, err)
	}
	return nil
}
