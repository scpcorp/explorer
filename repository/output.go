package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"math/big"
	"strings"

	"github.com/jackc/pgtype"
	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/explorer/domain"
)

const outputFields = `id, type, unlock_hash, coin_value, funda_value,
		fundb_value, maturity_height, output_block_id, output_height,
		input_block_id, input_height, output_transaction_id,
		input_transaction_id, contract_id, contract_revision`

const sqlAddOutput = `
INSERT INTO outputs (` + outputFields + `)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)
`

func (r *repository) AddOutput(ctx context.Context, output domain.Output) error {
	coin, err := currencyToPgNumeric(output.CoinValue)
	if err != nil {
		return fmt.Errorf("cannot convert CoinValue to pgtype.Numeric: %w", err)
	}

	funda64, err := output.FundaValue.Uint64()
	if err != nil {
		return fmt.Errorf("cannot convert FundaValue to uint64 Value=%s: %w", output.FundaValue.String(), err)
	}

	fundb64, err := output.FundbValue.Uint64()
	if err != nil {
		return fmt.Errorf("cannot convert FundbValue to uint64 Value=%s: %w", output.FundbValue.String(), err)
	}

	revisionNum, err := toPgNumeric(output.ContractRevision)
	if err != nil {
		return fmt.Errorf("cannot convert ContractRevision=%d to pgtype.Numeric: %w", output.ContractRevision, err)
	}

	_, err = r.db.ExecContext(ctx, sqlAddOutput,
		output.ID.String(),
		output.Type,
		output.UnlockHash.String(),
		coin,
		funda64,
		fundb64,
		uint64(output.MaturityHeight),
		output.OutputBlockID.String(),
		uint64(output.OutputHeight),
		toNullString(output.InputBlockID),
		toNullInt64(output.InputHeight),
		toNullString(output.OutputTransactionID),
		toNullString(output.InputTransactionID),
		toNullString(output.ContractID),
		revisionNum,
	)
	if err != nil {
		return fmt.Errorf("db: %w", err)
	}

	return nil
}

const sqlUseOutput = `
UPDATE outputs SET
	input_block_id = $2,
	input_height = $3,
	input_transaction_id = $4
WHERE id = $1 AND input_block_id IS NULL
RETURNING ` + outputFields

func (r *repository) UseOutput(ctx context.Context, outputID types.OutputID, blockID types.BlockID, height types.BlockHeight, transactionID *types.TransactionID) (domain.Output, error) {
	var transactionIDArg sql.NullString
	if transactionID != nil {
		transactionIDArg.Valid = true
		transactionIDArg.String = transactionID.String()
	}
	row := r.db.QueryRowContext(ctx, sqlUseOutput,
		outputID.String(),
		blockID.String(),
		uint64(height),
		transactionIDArg)
	return scanOutput(row)
}

const sqlDeleteOutputsInBlock = `
DELETE FROM outputs
WHERE output_block_id = $1
`

func (r *repository) DeleteOutputsInBlock(ctx context.Context, blockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlDeleteOutputsInBlock,
		blockID.String())
	return err
}

const sqlUnuseOutputsInBlock = `
UPDATE outputs
SET
	input_block_id = NULL,
	input_height = NULL,
	input_transaction_id = NULL
WHERE input_block_id = $1
`

func (r *repository) UnuseOutputsInBlock(ctx context.Context, inputBlockID types.BlockID) error {
	_, err := r.db.ExecContext(ctx, sqlUnuseOutputsInBlock,
		inputBlockID.String())
	return err
}

const sqlGetFundOutput = `
SELECT funda_value, fundb_value
FROM outputs
WHERE id = $1
`

func (r *repository) IsFundbOutput(ctx context.Context, outputID types.OutputID) (bool, error) {
	row := r.db.QueryRowContext(ctx, sqlGetFundOutput)

	var funda, fundb uint64
	if err := row.Scan(&funda, &fundb); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, domain.ErrNotFound
		}
		return false, fmt.Errorf("scan: %w", err)
	}
	if err := row.Err(); err != nil {
		return false, fmt.Errorf("rowErr: %w", err)
	}

	if funda == 0 && fundb == 0 {
		return false, fmt.Errorf("not a fund output: %s", outputID)
	}
	return fundb > 0, nil
}

const sqlUpdateOutput = `
UPDATE outputs
SET
	type = $2,
	unlock_hash = $3,
	coin_value = $4,
	funda_value = $5,
	fundb_value = $6,
	maturity_height = $7,
	output_block_id = $8,
	output_height = $9,
	input_block_id = $10,
	input_height = $11,
	output_transaction_id = $12,
	input_transaction_id = $13,
	contract_id = $14,
	revision = $15
WHERE id = $1
RETURNING ` + outputFields

func (r *repository) UpdateOutput(ctx context.Context, output domain.Output) (domain.Output, error) {
	coinValue, err := currencyToPgNumeric(output.CoinValue)
	if err != nil {
		return emptyOutput, fmt.Errorf("currencyToPgNumeric CoinValue=%s outputID=%s: %w", output.CoinValue, output.ID, err)
	}

	fundaValue, err := currencyToPgNumeric(output.FundaValue)
	if err != nil {
		return emptyOutput, fmt.Errorf("currencyToPgNumeric FundaValue=%s outputID=%s: %w", output.FundaValue, output.ID, err)
	}

	fundbValue, err := currencyToPgNumeric(output.FundbValue)
	if err != nil {
		return emptyOutput, fmt.Errorf("currencyToPgNumeric FundbValue=%s outputID=%s: %w", output.FundbValue, output.ID, err)
	}

	var inputHeight sql.NullInt64
	if output.InputHeight != nil {
		inputHeight.Valid = true
		inputHeight.Int64 = int64(*output.InputHeight)
	}

	var contractRevision pgtype.Numeric
	if output.ContractID != nil {
		contractRevision, err = toPgNumeric(output.ContractRevision)
		if err != nil {
			return emptyOutput, fmt.Errorf("toPgNumeric ContractRevision=%d: %w", output.ContractRevision, err)
		}
	}

	row := r.db.QueryRowContext(ctx, sqlUpdateOutput,
		output.ID.String(),
		output.UnlockHash.String(),
		coinValue,
		fundaValue,
		fundbValue,
		output.MaturityHeight,
		output.OutputBlockID.String(),
		uint64(output.OutputHeight),
		toNullString(output.InputBlockID),
		inputHeight,
		toNullString(output.OutputTransactionID),
		toNullString(output.InputTransactionID),
		toNullString(output.ContractID),
		contractRevision,
	)

	return scanOutput(row)
}

const sqlOutputByID = `
SELECT ` + outputFields + `
FROM outputs
WHERE id = $1`

func (r *repository) OutputByID(ctx context.Context, outputID types.OutputID) (domain.Output, error) {
	row := r.db.QueryRowContext(ctx, sqlOutputByID, outputID.String())
	return scanOutput(row)
}

const fmtSqlOutputsByIDs = `
SELECT ` + outputFields + `
FROM outputs
WHERE id IN (%s)`

func (r *repository) OutputsByIDs(ctx context.Context, outputIDs []types.OutputID) ([]domain.Output, error) {
	args := make([]string, 0, len(outputIDs))
	bind := make([]interface{}, 0, len(outputIDs))
	for i, o := range outputIDs {
		args = append(args, fmt.Sprintf("$%d", i+1))
		bind = append(bind, o.String())
	}

	query := fmt.Sprintf(fmtSqlOutputsByIDs, strings.Join(args, ","))
	rows, err := r.db.QueryContext(ctx, query, bind...)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}
	defer rows.Close()

	outputs := make([]domain.Output, 0, len(outputIDs))
	for rows.Next() {
		o, err := scanOutput(rows)
		if err != nil {
			return nil, fmt.Errorf("scan: %w", err)
		}
		outputs = append(outputs, o)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("rows: %w", err)
	}

	return outputs, nil
}

const sqlUnlockHashOutputHeight = `
SELECT output_height
FROM outputs
WHERE unlock_hash = $1
ORDER BY output_height
LIMIT 1
`

func (r *repository) UnlockHashOutputHeight(ctx context.Context, addr types.UnlockHash) (types.BlockHeight, error) {
	row := r.db.QueryRowContext(ctx, sqlUnlockHashOutputHeight, addr.String())
	if err := row.Err(); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, domain.ErrNotFound
		}
		return 0, fmt.Errorf("query: %w", err)
	}
	var height uint64
	if err := row.Scan(&height); err != nil {
		return 0, fmt.Errorf("scan: %w", err)
	}
	return types.BlockHeight(height), nil
}

type outputRow struct {
	ID             string
	Type           string
	UnlockHash     string
	CoinValue      pgtype.Numeric
	FundaValue     uint64
	FundbValue     uint64
	MaturityHeight uint64

	OutputBlockID       string
	OutputHeight        uint64
	InputBlockID        sql.NullString
	InputHeight         sql.NullInt64
	OutputTransactionID sql.NullString
	InputTransactionID  sql.NullString
	ContractID          sql.NullString
	ContractRevision    pgtype.Numeric
}

var emptyOutput = domain.Output{}

func scanOutput(row scanner) (domain.Output, error) {
	var r outputRow
	if err := row.Scan(
		&r.ID,
		&r.Type,
		&r.UnlockHash,
		&r.CoinValue,
		&r.FundaValue,
		&r.FundbValue,
		&r.MaturityHeight,
		&r.OutputBlockID,
		&r.OutputHeight,
		&r.InputBlockID,
		&r.InputHeight,
		&r.OutputTransactionID,
		&r.InputTransactionID,
		&r.ContractID,
		&r.ContractRevision,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return emptyOutput, domain.ErrNotFound
		}
		return emptyOutput, fmt.Errorf("scan: %w", err)
	}

	if err := row.Err(); err != nil {
		return emptyOutput, fmt.Errorf("rowErr: %w", err)
	}

	o := domain.Output{
		// ID: TBD
		Type: r.Type,
		// UnlockHash: TBD
		// CoinValue: TBD
		FundaValue:     types.NewCurrency64(r.FundaValue),
		FundbValue:     types.NewCurrency64(r.FundbValue),
		MaturityHeight: types.BlockHeight(r.MaturityHeight),

		// OutputBlockID: TBD
		OutputHeight: types.BlockHeight(r.OutputHeight),
		// InputBlockID: TBD
		// InputHeight: TBD
		// OutputTransactionID: TBD
		// InputTransactionID: TBD
		// ContractID: TBD
		// ContractRevision: TBD
	}

	var hash crypto.Hash
	err := hash.LoadString(r.ID)
	if err != nil {
		return emptyOutput, fmt.Errorf("output.ID.LoadString ID=%s: %w", r.ID, err)
	}
	o.ID = types.OutputID(hash)

	err = o.UnlockHash.LoadString(r.UnlockHash)
	if err != nil {
		return emptyOutput, fmt.Errorf("output.UnlockHash.LoadString UnlockHash=%s: %w", r.UnlockHash, err)
	}

	var s string
	err = r.CoinValue.AssignTo(&s)
	if err != nil {
		return emptyOutput, fmt.Errorf("cannot assign to string CoinValue=%v: %w", r.CoinValue, err)
	}
	b, ok := new(big.Int).SetString(s, 10)
	if !ok {
		return emptyOutput, fmt.Errorf("cannot SetString on *big.Int s=%s: %w", s, err)
	}
	o.CoinValue = types.NewCurrency(b)

	err = o.OutputBlockID.LoadString(r.OutputBlockID)
	if err != nil {
		return emptyOutput, fmt.Errorf("output.OutputBlockID.LoadString BlockID=%s: %w", r.OutputBlockID, err)
	}

	if r.InputBlockID.Valid {
		o.InputBlockID = &types.BlockID{}
		err = o.InputBlockID.LoadString(r.InputBlockID.String)
		if err != nil {
			return emptyOutput, fmt.Errorf("output.InputBlockID.LoadString BlockID=%s: %w", r.InputBlockID.String, err)
		}
	}

	if r.InputHeight.Valid {
		h := types.BlockHeight(uint64(r.InputHeight.Int64))
		o.InputHeight = &h
	}

	if r.OutputTransactionID.Valid {
		var h crypto.Hash
		err = h.LoadString(r.OutputTransactionID.String)
		if err != nil {
			return emptyOutput, fmt.Errorf("crypto.Hash.LoadString OutputTransactionID=%s: %w", r.OutputTransactionID.String, err)
		}
		txID := types.TransactionID(h)
		o.OutputTransactionID = &txID
	}

	if r.InputTransactionID.Valid {
		var h crypto.Hash
		err = h.LoadString(r.InputTransactionID.String)
		if err != nil {
			return emptyOutput, fmt.Errorf("crypto.Hash.LoadString InputTransactionID=%s: %w", r.InputTransactionID.String, err)
		}
		txID := types.TransactionID(h)
		o.InputTransactionID = &txID
	}

	if r.ContractID.Valid {
		var h crypto.Hash
		err = h.LoadString(r.ContractID.String)
		if err != nil {
			return emptyOutput, fmt.Errorf("crypto.Hash.LoadString ContractID=%s: %w", r.ContractID.String, err)
		}
		contractID := types.FileContractID(h)
		o.ContractID = &contractID

		var contractRevision uint64
		err := r.ContractRevision.AssignTo(&contractRevision)
		if err != nil {
			return emptyOutput, fmt.Errorf("pgtype.Numeric.AssignTo ContractRevision: %w", err)
		}
		o.ContractRevision = contractRevision
	}

	return o, nil
}
